import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: customer_input_info
    width: 1024
    height: 768
    property int timer_value: 60
    property var show_name:"-"
    property var show_email:"-"
    property var show_phone:"-"
    property var press_step: "0"
    property var show_count:0
    property var usageOf:"undefined"
    property var data_product:"-"
    property var total_amount:"0"
    property var post_result:"undefined"
    property var post_success: "False"
    property variant check_number: ['081', '088', '085', '087', '083', '089', '999', '082']
    property variant check_domain: [".com", ".net", ".co.id", ".asia", ".ac.id", ".id", ".com.my",
        ".my" ,".sg", ".ph", ".info", ".tv", ".sch.id", ".go.id"]

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log("usage for : " + usageOf)
            slot_handler.start_get_locker_name()
            if(customer_input_info.show_name != "" || customer_input_info.show_email != "" || customer_input_info.show_phone != ""){
                customer_input_info.show_name=""
                customer_input_info.show_email=""
                customer_input_info.show_phone=""
            }
            space_button.visible = true
            post_success = "False"
            nextok_button.text = qsTr("Lanjut")
            main_page.enabled = true
            textin.focus = true
            name_check.visible = false
            email_check.visible = false
            phone_check.visible = false
            touch_keyboard.alphaActive = true
            global_notif.close()
            press_step = "0"
            show_count = 0
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.start_post_subscribe_data_result.connect(post_data_result)
    }

    Component.onDestruction: {
        root.start_post_subscribe_data_result.disconnect(post_data_result)
    }

    function post_data_result(message){
        console.log("Post Subscribe data is ", message)
        loadingGif.close()
        if(message == "ERROR" || message == "FAILED"){
            txt_notif.text = qsTr("Oops, Terjadi kesalahan. Silakan coba lagi...")
            global_notif.open()
        }else{
            post_result = message
            if(post_result.indexOf("Exist") > -1){
                post_success = "Duplicate"
                img_global_notif.source = "img/otherImages/error_notif.png"
                //txt_notif.text = qsTr(post_result)
                txt_notif.text = qsTr("Oops, Alamat email tersebut sudah pernah didaftarkan, Silakan coba lagi...")
            }else{
                post_success = "True"
                img_global_notif.source = "img/otherImages/checklist_red2.png"
                txt_notif.font.pointSize = 25
                txt_notif.text = qsTr("Terima kasih sudah berpartisipasi. Kamu akan mendapatkan merchandise dari PopBox jika beruntung!")
            }
            global_notif.open()
        }
    }

    function validate_email(str) {
        var pattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(net|org|biz|com|edu|gov|info|net|asia|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
        return pattern.test(str)
     }

    function start_post_cust_data(){
        loadingGif.open()
        var cust_name = customer_input_info.show_name.toUpperCase()
        var cust_email = customer_input_info.show_email.toLowerCase()
        var cust_phone = customer_input_info.show_phone
        var cust_data = cust_email + "||" + cust_phone
        console.log("posting_data_customer : " + cust_name + "||" + cust_data)
        slot_handler.start_post_subscribe_data(cust_name, cust_data)
    }

    function validate_phone(number){
        if(check_number.indexOf(number.substring(0,3)) > -1 && number.length > 6){
            return true
        }else{
            return false
        }
    }

    function check_email(email){
        if(email.length > 6 && email.indexOf("@") > -1 && validate_email(email)==true){
            return true
        }else{
            return false
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
        id:select_service_back_button
        x:20
        y:20
        show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }

        }
    }

    Rectangle{
        id:main_page

        EmailKeyboard{
            id:touch_keyboard
            x:29
            y:360
            property var count:show_count
            alphaActive: true

            Component.onCompleted: {
                touch_keyboard.letter_button_clicked.connect(show_validate_code)
                touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
            }

            MouseArea{
                id: del_button_mouse_area
                x:857
                y:0
                height: 80
                width: 80
                onClicked: {
                    touch_keyboard.define_del_text()
                }
            }

            Rectangle{
                id: space_button
                x:273
                y:335
                width: 415
                height: 50
                color: "transparent"

                Text{
                    id: space_button_txt
                    anchors.fill: space_button
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                    text: qsTr("Spasi")
                    color:"#ffffff"
                    font.family:"Microsoft YaHei"
                    font.pixelSize:24
                }

                MouseArea{
                    anchors.fill: space_button
                    onClicked: {
//                        console.log("space is being pressed")
                        touch_keyboard.on_function_button_clicked("space")
                    }

                }
            }



            Rectangle{
                id: custom_ok_button
                x:273
                y:335
                width: 415
                height: 50
                color: "transparent"
                visible: (space_button.visible) ? false : true

                Text{
                    id: nextok_button
                    anchors.fill: custom_ok_button
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                    color:"#ffffff"
                    font.family:"Microsoft YaHei"
                    font.pixelSize:24
                }

                MouseArea{
                    anchors.fill: custom_ok_button
                    onClicked: {
                        touch_keyboard.on_function_button_clicked("ok")
                    }
                }
            }


            function define_del_text(){
//                console.log("press_step : " + press_step + ", with count : " + count)
                count -= 1
                if(press_step=="0"){
                    customer_input_info.show_name = customer_input_info.show_name.substring(0, customer_input_info.show_name.length-1)
                }else if(press_step=="1"){
                    customer_input_info.show_email = customer_input_info.show_email.substring(0, customer_input_info.show_email.length-1)
                }else if(press_step=="2"){
                    customer_input_info.show_phone = customer_input_info.show_phone.substring(0, customer_input_info.show_phone.length-1)
                }
            }

            function validate_email(str) {
                var pattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(net|org|biz|com|edu|gov|info|net|asia|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
                return pattern.test(str)
             }

            function on_function_button_clicked(str){
                if(str == "ok"){
                    //console.log("press_step : " + press_step)
                    if(textin.focus == true && customer_input_info.show_name.length > 3){
                        press_step = "1"
                        count = 0
                        space_button.visible = false
                        name_check.visible = true
                        textin.focus = false
                        emailin.focus = true
                        txt_notif.text = qsTr("Oops, Terjadi kesalahan. Pastikan alamat email Anda benar.")
                    }else if(press_step == "1" && check_email(customer_input_info.show_email)){
                        press_step = "2"
                        count = 0
                        nextok_button.text = "Send"
                        email_check.visible = true
                        emailin.focus = false
                        phonein.focus = true
                        touch_keyboard.alphaActive = false
                        txt_notif.text = qsTr("Oops, Terjadi kesalahan. Pastikan No. HP Anda benar.")
                        //start_post_cust_data()
                    }else if(press_step == "2" && validate_phone(customer_input_info.show_phone)){
                        count = 0
                        phone_check.visible = true
                        email_check.visible = true
                        emailin.focus = false
                        phonein.focus = true
                        start_post_cust_data()
                        slot_handler.start_video_capture("registration_" +customer_input_info.show_name.toUpperCase())
                    }else{
                        main_page.enabled = false
                        global_notif.open()
                    }
                }
                if(str=="delete"){
                    console.log("press_step : " + press_step)
                    if(count>=30){
                        count=29
                    }
                }
                if(str=="space"){
                    if(textin.focus){
                        customer_input_info.show_name += " "
                    }else{
                        return
                    }
                }
            }

            function show_validate_code(str){
                console.log("str : " + str)
                if (str == "" && count > 0 || str == " " && count > 0){
                    /*if(count >= 72){
                        count = 71
                    }*/
                    if(textin.focus == true && count>=30){
                        count=29
                    }else if(emailin.focus == true && count>=30){
                        count=29
                    }else if(phonein.focus == true && count>=30){
                        count=29
                    }
                    count--
                    if(textin.focus == true){
                        customer_input_info.show_name=customer_input_info.show_name.substring(0,count);
                    }else if(emailin.focus == true){
                        customer_input_info.show_email=customer_input_info.show_email.substring(0,count);
                    }else if(phonein.focus == true){
                        customer_input_info.show_phone=customer_input_info.show_phone.substring(0,count);
                    }
                }
                if (str != "" && count < 30){
                    count++
                }
                if (count >= 30){
                    str = ""
                }
                if(str=="space"){
                    if(textin.focus){
                        customer_input_info.show_name += " "
                    }else{
                        return
                    }
                }else{
                    if(textin.focus){
                        customer_input_info.show_name += str
                    }else if(emailin.focus){
                        customer_input_info.show_email += str
                    }else if(phonein.focus){
                        customer_input_info.show_phone += str
                    }
                }
                abc.counter = timer_value
                my_timer.restart()
            }
        }
    }

    Column{
        spacing: 23
        y:158
        x:105

        Text{
            id:name_label
            width: 150
            font.family:"Microsoft YaHei"
            text:qsTr("Nama")
            color:"#FFFFFF"
            font.pixelSize:32
        }

        Text{
            id:email_label
            width: 150
            font.family:"Microsoft YaHei"
            text:qsTr("Email")
            color:"#FFFFFF"
            font.pixelSize:32
        }

        Text{
            id:phone_label
            width: 150
            font.family:"Microsoft YaHei"
            text:qsTr("No. HP")
            color:"#FFFFFF"
            font.pixelSize:32
        }
    }

    Text{
        id:sign_optional_text
        y:286
        x:208
        width: 50
        font.family:"Microsoft YaHei"
        text:"(Optional)"
        color:"#FFFFFF"
        font.pixelSize:12
        font.italic: true
        visible: false
    }

    Rectangle{
        id: name_field
        x:295
        y:143
        width:625
        height:58
        color:"transparent"

        Image{
            anchors.fill: parent
            source:"img/courier11/input1.png"
        }

        TextInput{
            id:textin
            y:5
            width: 600
            x:20
            clip: true
            cursorVisible: (textin.focus) ? true : false
            font.family:"Microsoft YaHei"
            text:show_name
            color:"#FFFFFF"
            font.pixelSize:35
        }
    }

    Rectangle{
        id: email_field
        x:295
        y:212
        width:625
        height:58
        color:"transparent"

        Image{
            anchors.fill: parent
            source:"img/courier11/input1.png"
        }

        TextInput{
            id:emailin
            y:5
            width: 600
            height: 40
            x:20
            clip: true
            cursorVisible: (emailin.focus) ? true : false
            font.family:"Microsoft YaHei"
            text:show_email
            //text:"fitrah.wahyudi.imam@gmail.com"
            color:"#FFFFFF"
            font.pixelSize:35
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                touch_keyboard.on_function_button_clicked("ok")
            }
        }
    }

    Rectangle{
        id: phone_field
        x:295
        y:281
        width:400
        height:58
        color:"transparent"

        Image{
            width:400
            height:58
            source:"img/courier11/input1.png"
        }

        TextInput{
            id:phonein
            y:5
            clip: true
            cursorVisible: (phonein.focus) ? true : false
            x:20
            font.family:"Microsoft YaHei"
            text:show_phone
            color:"#FFFFFF"
            font.pixelSize:35
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                touch_keyboard.on_function_button_clicked("ok")
                /*press_step == "2"
                if(check_email(customer_input_info.show_email)){
                    touch_keyboard.count = 0
                    email_check.visible = true
                    emailin.focus = false
                    phonein.focus = true
                    touch_keyboard.alphaActive = false
                    txt_notif.text = qsTr("Oops, Something is wrong. Please ensure the phone number is correct.")
                }*/
            }
        }
    }

    GroupBox{
        id: validation_check
        x: 701
        y: 136
        width: 300
        height: 210
        flat: true
        Image{
            id: name_check
                visible: false
                x:237
                y:8
                source: "img/otherImages/checked_white.png"
                width: 40
                height: 40
                fillMode: Image.Stretch
        }

        Image {
            id: email_check
            visible: false
            x: 237
            y: 79
            width: 40
            height: 40
            fillMode: Image.Stretch
            source: "img/otherImages/checked_white.png"
        }

        Image {
            id: phone_check
            visible: false
            x: 7
            y: 147
            width: 40
            height: 40
            fillMode: Image.Stretch
            source: "img/otherImages/checked_white.png"
        }
    }

    LoadingView{
        id: loadingGif
    }

    Rectangle{
        id: opacity_bground
        anchors.fill: customer_input_info
        color: "#472f2f"
        opacity: 0.8
        visible: global_notif.visible
    }

    Rectangle{
        id:global_notif
        x: 90
        y: 200
        width: 850
        height: 450
        color: "#FFFFFF"
        opacity: 0.9
        radius: 25
        visible: false

        function open(){
            global_notif.visible = true
        }

        function close(){
            global_notif.visible = false
        }

        Image {
            id: img_global_notif
            x: 17
            y: 92
            width: 200
            height: 200
            source: "img/otherImages/x-mark_red.png"
            fillMode: Image.PreserveAspectFit
        }

        Text {
            id: txt_notif
            x: 247
            y:33
            width: 575
            height: 317
            text: qsTr("Oops, Terjadi kesalahan. Pastikan semua data terisi benar.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.capitalization: Font.Capitalize
        }

        OverTimeButton{
            id:global_notif_button
            x:286
            y:363
            show_text:qsTr("OK")
            show_x:15
            show_source : ""
            text_color: "white"
            bg_color: "red"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(post_success=="False"){
                        if(textin.focus == true){
                            customer_input_info.show_name = ""
                        }else if(emailin.focus == true){
                            customer_input_info.show_email = ""
                        }else if(phonein.focus == true){
                            customer_input_info.show_phone = ""
                        }
                        show_count = 0
                        abc.counter = timer_value
                        my_timer.restart()
                        main_page.enabled = true
                        global_notif.close()
                    }else if(post_success=="Duplicate"){
                        if(customer_input_info.show_name != "" || customer_input_info.show_email != "" || customer_input_info.show_phone != ""){
                            customer_input_info.show_name=""
                            customer_input_info.show_email=""
                            customer_input_info.show_phone=""
                        }
                        post_success = "False"
                        nextok_button.text = qsTr("Lanjut")
                        textin.focus = true
                        name_check.visible = false
                        email_check.visible = false
                        phone_check.visible = false
                        touch_keyboard.alphaActive = true
                        press_step = "0"
                        show_count = 0
                        abc.counter = timer_value
                        my_timer.restart()
                        main_page.enabled = true
                        global_notif.close()
                    }else{
                        my_timer.stop()
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    }
                }
            }
        }
    }
}
