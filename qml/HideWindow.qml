import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:floating_window
    visible: false

    Rectangle{
        x: 0
        y: 0
        width: 1024
        height: 768
        color: "#472f2f"
        opacity: 0.8

    }

    Rectangle{
        x: 90
        y: 200
        Image {
            id: image1
            x: 0
            y: 0
            width:860
            height: 500
            source: "img/courier13/layerground1.png"
        }
    }
    function open(){
        floating_window.visible = true
    }
    function close(){
        floating_window.visible = false
    }
}
