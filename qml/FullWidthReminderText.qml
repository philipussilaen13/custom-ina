import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:1024
    height:40
    color:"transparent"
    property var remind_color:""
    property var remind_text:""
    property var remind_text_size:""
    Text{
        text:remind_text
        font.family:"Microsoft YaHei"
        font.bold: true
        color:remind_color
        font.pixelSize:remind_text_size
        anchors.left: parent.left
        anchors.leftMargin: 50
    }
}
