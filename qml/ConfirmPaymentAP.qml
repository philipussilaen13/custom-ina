import QtQuick 2.4
import QtQuick.Controls 1.2
import 'base_function.js' as FUNC
import 'door_price.js' as PRICE

BaseAP{
    id: confirmAP
    property var press: '0'
    watermark: true
    topPanelColor: 'BLUE'
    mainMode: false
    property var phone_number: ''
    property var selectedPayment: ''
    property int timer_value: 60
    // property var duration: '8' // data lama
    property var duration: '' // data perubahan
    property var minutes: '0' // data perubahan
    property bool isExtending: false
    property var extendData: undefined
    property var popsafe_param: undefined
    property var product_class: undefined
    property int show_timer_value: 5
    property int show_timer_value_nf: 5
    property int trial_count: 0
    property var paymentResult: undefined
    property var door_no: ''
    property var extend_pincode: ''
    property var extend_number: ''
    property var locker_name: ''
    property var provider: ''
    property bool testingMode: false
    property var amount: '3000'
    property var dataOverdue: undefined
    property var popsafeParamFull: undefined
    property var imageLangkah: undefined
    property var globalResult: undefined
    signal sendManualTrigger(string str)


    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            abc.counter = timer_value;
            my_timer.start();    
            slot_handler.start_hour_overdue()
            if (popsafe_param != undefined) {
                var param_popsafe = JSON.parse(popsafe_param)      
                console.log(" popsafe_param_true: " + popsafe_param )
                
            }
            if (imageLangkah != undefined) {
                step_progress.source = imageLangkah
            }
            
            get_amount();
            if (isExtending==true){
                parse_data(extendData);
                imageLangkah = 'img/apservice/langkah/ambil03.png'
                tanggal();
            }
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop();
        }
    }

    function tanggal(){
        var datab = JSON.parse(extendData);
        var datax = JSON.parse(datab.overdue_data);    
        var tanggal = datax.date_overdue;    
        var tgl = new Date(tanggal);
        var penambahan = Qt.formatDateTime(tgl , "dd MMMM yyyy hh:mm");
        batas_text.contentText = penambahan;
    }

    function create_payment(){
        switch (selectedPayment){
            case 'EMONEY':
                console.log('emoney selected_confirm')
                console.log('Timer value ' + show_timer_value )
                payment_page.open();
                show_timer_payment.restart();
                break;
            case 'BNI-YAP':
                console.log('bni-yap selected');
                loadingPopUp.popupText = qsTr('Memproses...');
                loadingPopUp.open();
                var item = 'AP_EXPRESS @ ' + locker_name + ' : ' + door_no;
                var customer = 'USER_APEXPRESS||'+phone_number+'||a@a.com';
                slot_handler.start_create_payment_yap(amount, customer, item, locker_name)
                break;
            case 'MID-GOPAY':
                console.log('gopay selected');
                loadingPopUp.popupText = qsTr('Memproses...');
                loadingPopUp.open();
                var item = 'AP_EXPRESS @ ' + locker_name + ' : ' + door_no;
                var customer = 'USER_APEXPRESS||'+phone_number+'||a@a.com';
                slot_handler.start_create_payment_gopay(amount, customer, item, locker_name)
                break;
            case 'TCASH':
                console.log('tcash selected');
                loadingPopUp.popupText = qsTr('Memproses...');
                loadingPopUp.open();
                var item = 'AP_EXPRESS @ ' + locker_name + ' : ' + door_no;
                var customer = 'USER_APEXPRESS||'+phone_number+'||a@a.com';
                slot_handler.start_create_payment_tcash(amount, customer, item, locker_name)
                break;
            case 'OTTO-QR':
                console.log('otto qr selected');
                loadingPopUp.popupText = qsTr('Memproses...');
                loadingPopUp.open();
                var item = 'AP_EXPRESS @ ' + locker_name + ' : ' + door_no;
                var customer = 'USER_APEXPRESS||'+phone_number+'||a@a.com';
                slot_handler.start_create_payment_ottoqr(amount, customer, item, locker_name)
                break;
            default:
                break;
        }
    }


    Component.onCompleted: {
        sendManualTrigger.connect(manual_trigger);
        root.start_create_payment_result.connect(create_trans_global_result);
        root.choose_mouth_result.connect(select_door_result);
        root.mouth_number_result.connect(define_door_no);
        root.start_global_payment_emoney_result.connect(emoney_result);
        root.apexpress_result.connect(next_process);
        root.update_apexpress_result.connect(process_update);
        root.customer_take_express_result.connect(process_result);
        root.start_hour_overdue_result.connect(hour_overdue_result)
    }

    Component.onDestruction: {
        sendManualTrigger.disconnect(manual_trigger);
        root.start_create_payment_result.disconnect(create_trans_global_result);
        root.choose_mouth_result.disconnect(select_door_result)
        root.mouth_number_result.disconnect(define_door_no);
        root.start_global_payment_emoney_result.disconnect(emoney_result);
        root.apexpress_result.disconnect(next_process);
        root.update_apexpress_result.disconnect(process_update);
        root.customer_take_express_result.disconnect(process_result);
        root.start_hour_overdue_result.disconnect(hour_overdue_result)
    }

    function hour_overdue_result(text){
        duration = text
    }

    function get_amount(){
        if (testingMode==true){
            amount = '200';
        } else {
            console.log("isExtending: " + isExtending)

            if (!(isExtending)){
                amount = PRICE.popsafe_price;
                console.log(" amount_overdue: " + amount)    
            }
        }
    }

    function create_trans_global_result(result){
        console.log("create_trans_global_result is adalah ", result)
        globalResult = result
        if (result=="ERROR"||result=="FAILED"){
            loadingPopUp.close();
            dimm_connection.visible=true;
            back_button.enabled = false;
            
            //return
        } else {
            // global result
            if(popsafe_param != undefined) {
                var r = JSON.parse(result);
                var popsafe = JSON.parse(popsafe_param)
                popsafe.transactionRecord = r.transaction_id
                popsafe.paymentMethod = selectedPayment
                popsafe.paymentAmount = amount
                popsafe_param = JSON.stringify(popsafe)
            }

            console.log(" popsafe_param_detected: ", popsafe_param)

            if ((selectedPayment == 'EMONEY' && popsafe_param != undefined) || (selectedPayment == 'EMONEY' && isExtending == true)) {
                
                if (isExtending == true) {
                    customer_take_extend(result)
                    slot_handler.start_update_apexpress(extend_pincode, result);

                } else {
                    if (popsafe_param == undefined) {
                        var r = JSON.parse(result);
                        var popsafe = {
                            "transactionRecord": r.transaction_id,
                            "paymentAmount": amount
                        }
                        popsafe_param = JSON.stringify(popsafe)
                    }
                    var param_popsafe = JSON.parse(popsafe_param)
                    var lockerSize = popsafe_param.lockerSize
                    slot_handler.start_store_apexpress(phone_number, duration, param_popsafe.transactionRecord, selectedPayment, param_popsafe.paymentAmount, lockerSize, result);
                }
            } else if ( popsafe_param != undefined || isExtending == true) {
                var products = {'name': 'APEXPRESS_' + phone_number, 'qty': '1', 'total_amount': amount};
                
                if (popsafe_param != undefined) {
                    var param_popsafe = JSON.parse(popsafe_param)
                    var lockerSize = popsafe_param.lockerSize
                }
                my_stack_view.push(qr_payment_ap, {products: JSON.stringify(products), provider: provider ,
                global_trans: result, phone_number: phone_number, duration: duration, paymentResult: result,
                extend_pincode: extend_pincode, isExtending: isExtending, popsafe_param: popsafe_param, product_class: product_class, imageLangkah: imageLangkah, topPanelColor: topPanelColor, extendData: extendData, lockerSize:lockerSize, paymentParam:result});
            }
        }
    }

    
    function customer_take_extend(param) {
        // DATA EXTEND FOR RECORD TO POPSAFE
        var dataExtend = JSON.stringify({
            "extendPincode": extend_pincode,
            "transactionRecord": "",
            "paymentMethod": "",
            "duration": "",
            "extendTimes": "",
            "overdueTime": "",
            "cost_overdue": "",
            "isExtend": false
        })

        if (isExtending == true) {
            
            console.log(" extendData: " + extendData)
            
            var tmpExtend = JSON.parse(extendData);
            
            console.log(" extendData: " + tmpExtend)
            var tmpdataExtend =  JSON.parse(tmpExtend.overdue_data);
            var tmpTrx = JSON.parse(param)

            console.log(" is_extending____: " + isExtending)
            
            // DATA EXTEND FOR RECORD TO POPSAFE
            dataExtend = JSON.stringify({
                "extendPincode": extend_pincode,
                "transactionRecord": tmpTrx.transaction_id,
                "paymentMethod": provider,
                "duration": tmpdataExtend.parcel_duration,
                "minutes_duration": tmpdataExtend.parcel_minutes_duration,
                "extendTimes": tmpdataExtend.extend_overdue,
                "overdueTime": tmpdataExtend.date_overdue,
                "cost_overdue": tmpdataExtend.cost_overdue,
                "timestamp_duration": tmpdataExtend.timestamp_duration,
                "isExtend": isExtending
            })
        }
        
        console.log(" dataExtend: " + dataExtend)
        
        slot_handler.start_push_data_settlement(phone_number +'-'+extend_number+'-'+ 'AP_EXPRESS_EXTEND');
        slot_handler.start_video_capture("take_apexpress_" + extend_pincode);
        slot_handler.customer_take_express(dataExtend+'||AP_EXPRESS');
    }

    function process_update(u){
        console.log('process_update: ' + u);
        if (u=='Success'){    
            //  slot_handler.get_express_mouth_number();
            if (((selectedPayment=='emoney' || selectedPayment=='EMONEY') && (isExtending == false))){
                
                console.log(" not_extending____: " + isExtending)
                
                slot_handler.start_push_data_settlement(phone_number +'-'+extend_number+'-'+ 'AP_EXPRESS_EXTEND');
                slot_handler.start_video_capture("take_apexpress_" + extend_pincode);
                slot_handler.customer_take_express(extend_pincode+'||AP_EXPRESS');
            }
        }
    }

    function process_result(text){
        console.log('process_result_confirm : ', text)
        if (text=='Error'||text=='NotInput'){
            failure_notif.titleText = qsTr('Parcel Tidak Ditemukan');
            failure_notif.notifText = qsTr('Mohon Maaf, Silakan Masukkan Kode Pin Yang Benar.');
            failure_notif.open();
            return
        }
        var status = text.split('||')[0];
        var info = text.split('||')[1];
        var door_no = 0;
        console.log(" info: " + info)
        
        if (info != 'undefined') {
            console.log(" info???: " + info)
            var detail = JSON.parse(info);
            var d = JSON.parse(detail.transactionType)
            door_no = d.door_no
        } else {
            var tempDoor =  JSON.parse(info)
            door_no = tempDoor.door_no
        }
//        if (status=='Overdue'){
//            my_stack_view.push(select_payment_ap, {phone_number: detail.phone_number, isExtending: true, extendData: info});
//        }
        if (status=='Success'){
            my_stack_view.push(take_parcel_ap, {door_no: door_no});
        }
    }

    function next_process(t){
        console.log('next_process', t);
        loadingPopUp.close();
        if (t=='ERROR'){
            notif_text.contentNotif = qsTr('Terjadi Kesalahan, Silakan Hubungi Layanan Pelanggan.');
            notif_text.successMode = false;
            notif_text.open();
            return
        }
        var info = JSON.parse(t);
        var param = {
            'paymentType': selectedPayment,
            'duration': duration,
            'amount': amount,
            'paymentStatus': info.result,
            'pin_code': info.pin_code,
            'date': info.date,
            'door_no': info.door_no,
            'result': paymentResult,
            'phone_number': phone_number,
            'express': info.express
        };
        if ((selectedPayment=='emoney' || selectedPayment=='EMONEY') && isExtending == false){
            slot_handler.start_push_data_settlement(phone_number +'-'+ info.express +'-'+ 'AP_EXPRESS');
        }
        my_stack_view.push(open_door_ap, {summary: JSON.stringify(param)});
    }

    function select_door_result(r){
        console.log('select_door_result_confirm', r);
        switch (r){
        case 'NotMouth':
            notif_text.successMode = false;
            notif_text.contentNotif = qsTr('Mohon Maaf, Tidak Ada Loker Yang Tersedia Saat Ini.');
            notif_text.open();
            break;
        case 'Success':
                // var param_popsafe = JSON.parse(popsafe_param)
                
                // console.log(" popsafe_param_success : " + popsafe_param)
                
                // slot_handler.start_store_apexpress(phone_number, duration, param_popsafe.transactionRecord, selectedPayment, param_popsafe.paymentAmount);
            
                // slot_handler.get_express_mouth_number();
            break;
        default:
            break;
        }
    }

    function define_door_no(d){
        console.log('define_door_no', d);
        door_no = d;
    }

    /*
          extendData = '{"id": "fc6852c09ba211e89941509a4ccea7f5", "mouth_id": "4028808359ff616b015a172842265752", "amount": 2000, "express_id": "fb4f280a9ba211e8be26509a4ccea7f5", "createTi
    me": 1533798495000, "transactionType": "{\"express\": \"APXID-9M2VX\", \"pin_code\": \"E3S9VC\", \"result\": \"SUCCESS\", \"door_no\": 44, \"phone_number\": \"085710157057\", \"date\": \"2018-08-09
     22:08:13\"}", "paymentType": "E3S9VC"}'
    */


    function parse_data(e){
        console.log('parse_date', e);
        var info = JSON.parse(e);
        extend_pincode = info.paymentType
        var detail = JSON.parse(info.transactionType)
        door_no = detail.door_no
        extend_number = detail.express
        var data_overdue = JSON.parse(dataOverdue)   
        amount = data_overdue.cost_overdue
        duration = data_overdue.parcel_duration
        minutes = data_overdue.parcel_minutes_duration
        duration_text.contentText = duration + ' Jam ' + minutes + ' Menit '
        
    }

    function emoney_result(result){
        // var result = '{"locker": "APS Simulation", "card_no": "6032984033304886", "last_balance": "28183", "raw": "0000603298403330488601200000000100028183271218104328|1545882207000", "terminal_id": "03330100", "show_date": "1545882207000"}';
        console.log('emoney_payment_result', result);

        if(result == "UNFINISHED"){
            trial_count += 1;
            payment_page.close();
            nonfinished_notif.open();
            show_timer_payment.stop();
            show_timer_nf.restart();
            ok_button.enabled = false;
            trial_count_text.text = trial_count;
            time_nf.counter = show_timer_value_nf;
            if(trial_count >= 4){
                //slot_handler.start_reset_partial_transaction()
                my_timer.stop();
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }));
            }
        }else if(result == "WRONG-CARD" || result == "ERROR" || result == "FAILED"){
            payment_page.close();
            ok_button.enabled = true;
            failure_notif.notifText = qsTr('Pastikan Saldo Kartu eMoney Anda mencukupi');
            failure_notif.open();
            return
        }

        var obj = JSON.parse(result)
        if(obj.length <= 0){
            payment_page.close();
            ok_button.enabled = false;
            failure_notif.notifText = qsTr('Pastikan Saldo Kartu eMoney Anda mencukupi');
            failure_notif.open();
            return
        }else{
            // loadingPopUp.popupText = qsTr('Memproses...');
            // loadingPopUp.open();
            paymentResult = result;
            var item = 'AP_EXPRESS @ ' + locker_name + ' : ' + door_no;
            var customer = 'USER_APEXPRESS||'+phone_number+'||a@a.com';
            slot_handler.start_create_payment_emoney(amount, customer, item, locker_name, result)
            
            console.log(" isExtending____: " + isExtending)
            
            if (isExtending==false){
                if (testingMode==true){
                    slot_handler.start_choose_mouth_size("M","staff_store_express");
                } else {
                    slot_handler.start_choose_mouth_size("L","staff_store_express");
                }
            } 
            // else {
            //     slot_handler.start_update_apexpress(extend_pincode);
            // }
        }
    }

    function manual_trigger(command){
        console.log('manual_trigger : ', command);
        if (command=='store_apexpress'){
            if (testingMode==true){
                slot_handler.start_choose_mouth_size("M","staff_store_express");
            } else {
                slot_handler.start_choose_mouth_size("L","staff_store_express");
            }
        }
    }


    BackButtonAP{
        id:back_button
        x:20
        y:20

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
        }
    }

    Rectangle{
        id: timer_set
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Image {
        id: step_progress
        anchors.top: parent.top
        anchors.topMargin: 115
        anchors.horizontalCenter: parent.horizontalCenter
        source: "img/apservice/langkah/2.png"
    }

    Text {
        id: main_command_text
        width: 500
        height: 100
        color: "#323232"
        text: qsTr("Rincian Pesanan")
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.top: parent.top
        anchors.topMargin: 200
        font.bold: true
        font.family: 'Microsoft YaHei'
        font.pixelSize: 50

    }

    Image {
        id: icon_confirm
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -10
        anchors.right: parent.right
        anchors.rightMargin: 10
        source: "img/apservice/img/rincian_pesanan_biru.png"
    }

    Column{
        id: details_text
        anchors.verticalCenterOffset: 10
        anchors.left: parent.left
        anchors.leftMargin: 44
        anchors.verticalCenter: parent.verticalCenter
        spacing: 0

        RowTextAP{
            id: no_hp_text
            labelText: qsTr('No HP')
            contentText: phone_number
            colorContent: '#323232'
            contentBold: true
            separatorPoint: getSeparator()//(!isExtending) ? 120 : 320
            textMargin: getMargin()//(!isExtending) ? 150 : 350
        }
        RowTextAP{
            id: batas_text
            visible: (!isExtending) ? false : true
            labelText: qsTr('Batas Pengambilan')
            contentText: qsTr('7 Juni 2018')
            colorContent: '#323232'
            contentBold: false
            separatorPoint: getSeparator()//(!isExtending) ? 120 : 320
            textMargin: getMargin()//(!isExtending) ? 150 : 350
        }
        RowTextAP{
            id: duration_text
            labelText: (!isExtending) ? qsTr('Durasi') : qsTr('Durasi Perpanjangan')
            contentText: duration + ' Jam '
            colorContent: '#323232'
            contentBold: false
            separatorPoint: getSeparator()//(!isExtending) ? 120 : 320
            textMargin: getMargin()//(!isExtending) ? 150 : 350
        }
        RowTextAP{
            id: amount_text
            labelText: qsTr('Jumlah')
            contentText: 'Rp '+FUNC.insert_dot(amount)
            colorContent: '#009BE1'
            contentBold: true
            separatorPoint: getSeparator()//(!isExtending) ? 120 : 320
            textMargin: getMargin()//(!isExtending) ? 150 : 350
        }
        RowTextAP{
            id: payment_text
            labelText: qsTr('Bayar')
            contentText: selectedPayment.toLocaleUpperCase()
            colorContent: '#323232'
            contentBold: false
            separatorPoint: getSeparator()//(!isExtending) ? 120 : 320
            textMargin: getMargin()//(!isExtending) ? 150 : 350
        }
    }

    function getSeparator(){
        var separPoint = 0
        if(isExtending==false) separPoint = 120
        else separPoint = 320
        return separPoint
    }

    function getMargin(){
        var textMargin = 0
        if(isExtending==false) textMargin = 150
        else textMargin = 350
        return textMargin      
    }
    
    Text{
        id: text_style_checkbox
        width: 600
        height: 100
        font.family: 'Microsoft YaHei'
        color: '#009BE1'
        text: qsTr('Dengan mengklik tombol Konfirmasi Anda telah setuju dengan Syarat dan Ketentuan yang berlaku.')
        anchors.verticalCenterOffset: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 50
        wrapMode: Text.WordWrap
        font.pixelSize: 23
        font.italic: true
    }

    NotifButtonAP{
        id: ok_button
        x: 50
        y: 644
        width: 300
        buttonColor: 'BLUE'
        modeReverse: false
        buttonText: qsTr('KONFIRMASI')
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(isExtending){
                    create_payment()
                }else{
                    dimm_act.visible=true;
                    rec_trans.visible=true;
                    abc.counter = timer_value;
                }
            }
        }
    }

    Notification{
        id: notif_text
        contentNotif: qsTr('Pastikan Nomor Telepon Anda adalah Benar.')
        successMode: false
    }

    LoadingPopUp{
        id: loadingPopUp
        z: 99
    }

    PaymentWindow{
        id:payment_page
//        visible:true
        useBaseOpacity: true
        globalOpacity: 1

        Text {
            id: text_payment_window
            x:334
            y:257
            width: 561
            height: 140
            text: qsTr("Tempelkan Kartu eMoney Anda di reader.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pixelSize:30
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_payment
                property int counter
                Component.onCompleted:{
                    time_payment.counter = show_timer_value
                }
            }
            Row{
                x:548
                y:399
                spacing:5
                Text {
                    id:show_time_payment_text
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    textFormat: Text.PlainText
                    font.pixelSize:25
                    wrapMode: Text.WordWrap
                }
                Text {
                    id:show_seconds_left_text
                    color: "#ab312e"
                    text: qsTr("detik")
                    font.family:"Microsoft YaHei"
                    font.pixelSize: 25
                }
            }

            Timer{
                id:show_timer_payment
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    
                    show_time_payment_text.text = time_payment.counter
                    time_payment.counter -= 1
                    if(time_payment.counter == 2){
                        slot_handler.start_global_payment_emoney(amount);
                        //my_stack_view.push(on_develop_view)
                    }
                    if(time_payment.counter < 2){
                        show_timer_payment.stop()
                        time_payment.counter = 5
                        // show_time_payment_text.visible = false
                        // show_seconds_left_text.visible = false
                        ok_button.enabled = true;
                        abc.counter = timer_value
                    }
                }
            }
        }
    }

    PaymentWindow{
        id:nonfinished_notif
//        visible:true
        useBaseOpacity: true
        globalOpacity: 1


        Text {
            id: text_nonfisnihed_1
            x:130
            y:218
            width: 765
            height: 50
            text: qsTr("Transaksi Gagal")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pixelSize:40
            wrapMode: Text.WordWrap
        }

        Text {
            id: text_nonfisnihed_2
            x:340
            y:285
            width: 555
            height: 88
            text: qsTr("Tempelkan Kembali Kartu eMoney Anda.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pixelSize:30
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_nf
                property int counter
                Component.onCompleted:{
                    time_nf.counter = show_timer_value_nf
                }
            }
            Row{
                x:563
                y:391
                spacing:5
                Text {
                    id:show_time_nf
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    textFormat: Text.PlainText
                    font.pixelSize:40
                    wrapMode: Text.WordWrap
                }
                Text {
                    id: second_left_nf
                    color: "#ab312e"
                    text: qsTr("detiks")
                    font.family:"Microsoft YaHei"
                    font.pixelSize: 25
                }
            }
            Row{
                x:862
                y:520
                width: 30
                height: 30
                spacing:5
                Text {
                    id:trial_count_text
                    width: 30
                    height: 30
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    textFormat: Text.PlainText
                    font.pixelSize:20
                    wrapMode: Text.WordWrap
                }
            }

            Timer{
                id:show_timer_nf
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    show_time_nf.text = time_nf.counter
                    time_nf.counter -= 1
                    if(time_nf.counter == 2){
                        slot_handler.start_global_payment_emoney(amount);
                        //my_stack_view.push(on_develop_view)
                    }
                    if(time_nf.counter < 0){
                        show_timer_nf.stop()
                        show_time_nf.visible = false
                        second_left_nf.visible = false
                    }
                }
            }
        }
    }

    // Change Below NOtif to Use New
    HideWindow{
        id:wrong_card_notif
//        visible:true

        Text {
            id: text_wc_1
            x: 93
            y:464
            width: 850
            height: 50
            text: qsTr("Transaction Failed")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:28
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            id: text_wc_2
            x: 93
            y:520
            width: 850
            height: 50
            text: qsTr("Please use Mandiri e-Money prepaid card.")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:25
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            id: img_wrong_card_notif
            x: 410
            y: 235
            width: 215
            height: 215
            fillMode: Image.PreserveAspectFit
            source: "img/otherservice/no_credit.png"
        }

        OverTimeButton{
            id:wrong_card_notif_back
            x:378
            y:589
            show_text:qsTr("Cancel")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
                }
            }
        }
    }

    FailurePaymentAP{
        id: failure_notif
        titleText: qsTr('Pembayaran Gagal')
        notifText: qsTr('Mohon Maaf, Transaksi Anda gagal. Silakan Ulangi Pembayaran.')
    }

    Notification{
        id: notif_text_failed
        contentNotif: qsTr('Mohon Maaf, Pembayaran Tidak dapat dilakukan dengan payment channel yang anda pilih.')
        successMode: false
        redImage: true
    }

    Rectangle{
        id:dimm_act
        visible:false
        width: 1024
        height: 768
        color: "#AA4A4A4A"
    }

    Rectangle{
        id: rec_trans
        width: 800
        height: 587
        //border.color: "black"
        //color: "transparent"
        color:"white"
        radius: 22
        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.left: parent.left
        anchors.leftMargin: 100
        visible: false

        Rectangle {
            id: btn_batal
            anchors.left: parent.left
            anchors.leftMargin: 180
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 80
            visible:true
            Image {
                id: btl_logo
                width: 200
                height: 60
                source: "img/courier08/08ground_field.png"

                Text{
                    anchors.horizontalCenter : parent.horizontalCenter
                    anchors.verticalCenter : parent.verticalCenter
                    font.family: 'Microsoft YaHei'
                    font.pixelSize: 20
                    color: "#009BE1"
                    font.bold: true
                    text: qsTr("Batalkan")
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        //main_page.enabled = true;
                        press = '0';
                        //yap_status_notif.visible = false;
                        dimm_act.visible = false;
                        rec_trans.visible=false;
                        //cancel_button.enabled=true;
                        //confirmAP.visible = true;
                    }
                }
            }
        } 

        Rectangle {
            id: btn_setuju
            anchors.right: parent.right
            anchors.rightMargin: 370
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 80
            //visible: true
        Image {
            width: 200
            height: 60
            source: "img/courier08/08ground.1.png"

            Text{
                anchors.horizontalCenter : parent.horizontalCenter
                anchors.verticalCenter : parent.verticalCenter
                font.family: 'Microsoft YaHei'
                font.pixelSize: 20
                color: "#ffffff"
                font.bold: true
                text: qsTr("Setuju")
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    //if (press!='0') return
                    //press = '1'
                    dimm_act.visible=false;
                    rec_trans.visible=false;
                    create_payment()
                }
            }
        }
    }   
        Text{
            id: text_pop_up
            x:10
            y:10
            font.family: 'Microsoft YaHei'
            font.pixelSize: 20
            font.bold: true
            text: qsTr('Syarat dan Ketentuan Titip Barang di Loker PopBox')
        }

        ScrollView {
            id: sc_view_
            anchors.top: parent.top
            anchors.topMargin: 50
            anchors.left: parent.left
            anchors.leftMargin: 10
            width: 780
            height: 450
            Row{
                spacing: 5
                Text{
                    id: text_pop_body2
                    text: qsTr('Dengan ini saya setuju bahwa saya telah sepenuhnya mengerti dan mematuhi peraturan tata cara penggunaan service layanan ‘PopSafe’.\n\nSaya setuju untuk tidak menggunakan layanan PopSafe untuk:\n\n - Uang & perhiasan (dalam bentuk kertas maupun benda berharga lainnya)\n - Narkotika dan segala substansi psikoatif lainnya\n - Benda berharga (barang antik, akta, surat tanah, ijazah, dll)\n - Binatang, tanaman atau lainnya\n - Benda benda pornografi\n - Barang yang segar (buah, sayur, daging, lainnya)\n - Barang pecah belah\n - Senjata, amunisi, benda lainnya yang mudah terbakar/meledak\n - Barang yang basah, bocor, menimbulkan bau dan lainnya\n - Barang yang dapat menyebabkan resiko kesehatan, kerusakan dan lainnya\n - Barang lainnya yang dilarang sesuai oleh peraturan Undang-Undang Republik Indonesia\n - Barang yang memerlukan ijin khusus dalam pengiriman\n\nApabila saya melanggar tata cara penggunaan, maka saya bersedia untuk bertanggung jawab sepenuhnya atas segala kerugian yang timbul akibat kelalaian saya baik secara materiil atau imateriil. Dan saya bersedia untuk melakukan penggantian rugi sesuai dengan jumlah kerugian dan juga melalukan permintaan maaf secara publik apabila dibutuhkan.\n\nSaya paham bahwa satu order hanya berlaku maksimal '+ duration +' jam penyimpanan dan apabila saya ingin mengambil lewat dari '+ duration +' jam maka harus melakukan pembayaran atas perpanjangan order saya.\n\nSaya paham bahwa kode PIN yang diberikan bersifat rahasia dan saya tidak akan menuntut atau meminta ganti rugi kepada pihak PopBox atas kelalaian saya apabila saya baik secara sengaja maupun tidak sengaja memberikan kode PIN tersebut kepada orang lain.\n\nPopBox memiliki hak untuk membuka loker atas barang yang tidak diambil melebihi dari 1x24 jam tanpa ada status perpanjangan dari pelanggan tanpa konfirmasi terlebih dahulu dengan pemilik barang. PopBox berhak untuk mengambil dan memindahkan barang tersebut atau memusnahkan barang tersebut apabila tidak ada konfirmasi dalam 7 hari kalender ke pihak PopBox tanpa memberikan ganti rugi atau penggantian barang kepada pelanggan.')
                    font.family: 'Microsoft YaHei'
                    font.pixelSize: 16
                    width: 750
                    textFormat: Text.PlainText
                    wrapMode: Text.WrapAnywhere
                    verticalAlignment: Text.AlignVCenter
                }
            }
        }   
    }
    Rectangle{
        id:dimm_connection
        visible:false
        width: 1024
        height: 768
        color: "#AA4A4A4A"

        Rectangle{
            id: rec
            width: 600
            height: 460
            //border.color: "black"
            //color: "transparent"
            color:"white"
            radius: 22
            anchors.top: parent.top
            anchors.topMargin: 150
            anchors.left: parent.left
            anchors.leftMargin: 218

            Image{
                id: no_wifi
                width: 180
                height: 180
                anchors.top: parent.top
                anchors.topMargin: 50
                anchors.horizontalCenter: parent.horizontalCenter
                source: "img/apservice/no-wifi.png"

            }

            Text {
                color: "#A6B1B3"
                text: qsTr("Connection Issue")
                anchors.horizontalCenter: parent.horizontalCenter

                // anchors.left: parent.left
                // anchors.leftMargin:60
                anchors.top: parent.top
                anchors.topMargin: 250
                font.family: 'Microsoft YaHei'
                font.pixelSize: 50
                font.bold: true
            }

            Rectangle {
                id: btn_kembali
                anchors.left: parent.left
                anchors.leftMargin: 60
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 110
                visible:true
                Image {
                    id: btl_kembali
                    width: 200
                    height: 60
                    source: "img/courier08/08ground_field.png"

                    Text{
                        anchors.horizontalCenter : parent.horizontalCenter
                        anchors.verticalCenter : parent.verticalCenter
                        font.family: 'Microsoft YaHei'
                        font.pixelSize: 20
                        color: "#009BE1"
                        font.bold: true
                        text: qsTr("BATALKAN")
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            dimm_connection.visible = false;
                            back_button.enabled = true;
                            my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                        }
                    }   
                }
            } 

            Rectangle {
                id: btn_cobalagi
                anchors.right: parent.right
                anchors.rightMargin: 260
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 110
                //visible: true
                Image {
                    width: 200
                    height: 60
                    source: "img/courier08/08ground.1.png"

                    Text{
                        anchors.horizontalCenter : parent.horizontalCenter
                        anchors.verticalCenter : parent.verticalCenter
                        font.family: 'Microsoft YaHei'
                        font.pixelSize: 20
                        color: "#ffffff"
                        font.bold: true
                        text: qsTr("COBA LAGI")
                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            create_payment()
                            dimm_connection.visible=false;
                            back_button.enabled = true;
                        }
                    }
                }
            }   
        }
    }
}