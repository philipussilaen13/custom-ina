import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    property var show_text:"A"
    width:80
    height:80
    color:"white"
    border.color: "gray"
    border.width: 3
    radius: 10

    Text{
        text:show_text
        color:"#323232"
        font.family:"Microsoft YaHei"
        font.pixelSize:40
        anchors.centerIn: parent;
    }

}
