import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: base_bground
    width: 1024
    height: 768
    property int timer_value: 60
    property var press: "0"
    property real xpos
    property real ypos
    property var drawSign
    property var pinCode
    property var expressNumber
    property var resetButton: "0"
    property var pathImage: "signature/"
    property var usageOf

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            resetGlobalTimer()
            press = "0"
            resetButton = "0"
            continue_button.enabled = true
            cancel_button.enabled = true
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        //root.start_capture_signature_result.connect(resultEsign)
        //root.customer_take_express_result.connect(process_result)
    }

    Component.onDestruction: {
        root.start_capture_signature_result.disconnect(resultEsign)
        root.customer_take_express_result.disconnect(process_result)
    }

    function resultEsign(text){
        console.log('status start_capture_esignature : ' + text)
        if(text == "ERROR"){
            return
        }
        if(text == "SUCCESS"){
            slot_handler.customer_take_express(pinCode)
        }
    }

    function process_result(text){
        console.log('status customer_take_express_result : ' + text)
        switch(text){
        case "Success" : my_stack_view.push(customer_take_express_opendoor_view)
            break;
        case "Overdue" : my_stack_view.push(customer_take_express_overtime_view)
            break;
        case "Error" : my_stack_view.push(customer_take_express_error_view, {usageOf:usageOf})
            break;
        default : my_stack_view.push(customer_take_express_error_view, {usageOf:usageOf})
        }
    }

    function resetGlobalTimer(){
        abc.counter = timer_value
        my_timer.restart()
    }

    Rectangle{
        id: rec_timer
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Text {
        id: text_notif_1
        x: 0
        y: 142
        width: 1024
        height: 80
        text: qsTr("Please enter your signature")
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        font.pixelSize: 35
    }

    Rectangle {
        id: rec_preview
        x: 223
        y: 250
        width: 500
        height: 350
        color: "#ffffff"
        anchors.horizontalCenter: text_notif_1.horizontalCenter

        Canvas {
            id: myCanvas
            property int touch: 1
            width: rec_preview.width
            height: rec_preview.height
            antialiasing: false
            smooth: true
            onPaint: {
                drawSign = getContext('2d');
                drawSign.lineJoin = 'round';
                drawSign.lineCap = 'round';
                //drawSign.shadowColor = 'black';
                //drawSign.shadowBlur = 2;
                drawSign.strokeStyle = "black";
                drawSign.lineWidth = 3;
                if(touch%2==0){
                    drawSign.lineTo(xpos, ypos)
                    drawSign.stroke();
                }else{
                    drawSign.fillStyle = 'white';
                    drawSign.fillRect(xpos, ypos, drawSign.lineWidth, drawSign.lineWidth);
                }
            }

            function resetPressed(){
                if(resetButton == "1"){
                    drawSign.reset();
                    drawSign.clearRect(0, 0, myCanvas/width, myCanvas.height)
                    myCanvas.requestPaint()
                }else{
                    return
                }
                resetButton = "0"
                touch = 1
            }

            MouseArea{
                id: area
                anchors.fill: parent
                onPressed: {
                    resetGlobalTimer()
                    press = "1"
                    myCanvas.touch += 1
                    xpos = area.mouseX
                    ypos = area.mouseY
                    myCanvas.requestPaint()
                }
                /*onMouseXChanged: {
                    resetGlobalTimer()
                    press = "1"
                    xpos = mouseX
                    ypos = mouseY
                    myCanvas.requestPaint()
                }
                onMouseYChanged: {
                    resetGlobalTimer()
                    press = "1"
                    xpos = mouseX
                    ypos = mouseY
                    myCanvas.requestPaint()
                }*/
                onPositionChanged: {
                    resetGlobalTimer()
                    press = "1"
                    myCanvas.touch += 1
                    xpos = area.mouseX
                    ypos = area.mouseY
                    myCanvas.requestPaint()
                }
            }

            OverTimeButton{
                id:continue_button
                x:272
                y:388
                show_text:qsTr("Continue")
                show_x:15

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press != "1"){
                            return
                        }
                        continue_button.enabled = false
                        cancel_button.enabled = false
                        slot_handler.start_capture_signature(pathImage, pinCode + '_' + expressNumber)
                    }
                    onEntered:{
                        continue_button.show_source = "img/bottondown/down_1.png"
                    }
                    onExited:{
                        continue_button.show_source = "img/button/7.png"
                    }
                }
            }

            OverTimeButton{
                id:cancel_button
                x:-44
                y:388
                show_text:qsTr("Reset")
                show_x:15

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        resetButton = "1"
                        if(press == "1"){
                            myCanvas.resetPressed()
                        }else{
                            return
                        }
                    }
                    onEntered:{
                        cancel_button.show_source = "img/bottondown/down_1.png"
                    }
                    onExited:{
                        cancel_button.show_source = "img/button/7.png"

                    }
                }
            }
        }
    }
}
