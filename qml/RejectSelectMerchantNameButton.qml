import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{

    width:244
    height:190
    color:"transparent"

    property var show_image:""

    Image{
        x:0
        y:0
        width:244
        height:190
        source:show_image
    }

}
