import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{

    width:187
    height:168
    color:"transparent"

    property var show_text:""
    property var show_image:""
    property var show_source:"img/button/10.png"


    Image{
        x:0
        y:0
        width:187
        height:186
        source:show_source
    }


    Image{
        x:0
        y:0
        width:187
        height:186
        source:show_image
    }
}
