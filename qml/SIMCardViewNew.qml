import QtQuick 2.0

Rectangle{
    id: rectangle
    property var init_price: "100.000"
    property var phone_no: "08121234567"
    property var new_price: "97.500"
    property var operator: "telkomsel"
    property var sim_type: "Regular"
    property var product_desc: "Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum"
    width: 200
    height: 200
    radius: 15

    Image {
        id: base_image
        anchors.verticalCenterOffset: 10
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        scale: 0.25
        fillMode: Image.PreserveAspectFit
        source: "img/ppob/sim_card.png"
    }

    function get_logo(o){
        switch(o){
        case 'telkomsel':
            return "img/sepulsa/logo/telkomsel.png"
        case 'indosat':
            return "img/sepulsa/logo/indosat.png"
        case 'xl':
            return "img/sepulsa/logo/xl.png"
        case 'bolt':
            return "img/sepulsa/logo/bolt.png"
        default:
            return "img/sepulsa/logo/simpati.png"
        }
    }

    Image {
        x: 0
        source: get_logo(operator)
//        source: "img/sepulsa/logo/indosat.png"
        width: 100
        height: 50
        anchors.left: parent.left
        anchors.leftMargin: -10
        scale: 0.6
        fillMode: Image.PreserveAspectFit
    }

    Rectangle{
        width: 60
        height: 20
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.right: parent.right
        anchors.rightMargin: 20
        color: 'orange'
        Text {
            id: prod_type_text
            color: "#403a3a"
            text: sim_type
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            font.family: 'Microsoft YaHei'
            font.pixelSize: 10
            font.bold: true
        }
    }

    Text {
        id: phone_no_text
        text: insert_space(phone_no)
        anchors.verticalCenterOffset: 45
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.family: 'Microsoft YaHei'
        font.pixelSize: 18
        font.bold: true
        color: 'black'
    }


    Text {
        id: init_price_text
        text: init_price
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 25
        anchors.horizontalCenter: parent.horizontalCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.family: 'Microsoft YaHei'
        font.pixelSize: 8
        font.bold: false
        font.strikeout: true
        font.italic: true
        color: 'darkred'
    }

    Text {
        id: final_price_text
        text: new_price
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 7
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.family: 'Microsoft YaHei'
        font.pixelSize: 16
        font.bold: true
        color: 'darkred'
    }

    Text {
        id: prod_desc_text
        text: product_desc
        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width - 20
        height: 32
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.family: 'Microsoft YaHei'
        font.pixelSize: 9
        font.bold: false
        font.italic: true
        color: 'gray'
    }

    function insert_space(str){
        return str.replace(/(\d{4})/g, '$1 ').replace(/(^\s+|\s+$)/,'')
    }


}
