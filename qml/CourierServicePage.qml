import QtQuick 2.4
import QtQuick.Controls 1.2

BaseAP{
    id: baseAP
    watermark: true
    topPanelColor: 'BLUE'
    mainMode: false
    property int timer_value: 60
    property var press: "0"
    property var c_access: "full"
    property var fromExpress: "No"
    property var pref_login_user: "undefined"
    property var courier_name: "undefined"
    property var courier_username: "undefined"
    property var courier_com: "undefined"
    property var courier_slice: "undefined"
    property var txt_color: "#026FA0"
    property var txt_button_color: "#ffffff"
    property var txt_inactive: "#656565"
    property var buttonY: 280
    property var buttonY2: 520
    property var buttonXstore: 50 //1
    property var buttonXtake: 290 //2
    property var buttonXoverdue: 530 //3
    property var buttonXreject: 770 //4

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log('courier_credential : ' + c_access + ', with prefix_user_account : ' + pref_login_user)
            slot_handler.get_user_info()
            slot_handler.start_load_courier_overdue_express_count()
            // courier_take_package_button.show_source = "img/button/1.png"
            // courier_take_overdue_button.show_source = "img/button/1.png"
            // courier_take_it_button.show_source = "img/button/1.png"
            // // history_button.show_source = "img/button/1.png"
            // // other_services_button.show_source = "img/button/1.png"
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.user_info_result.connect(detail_user)
        root.overdue_express_count_result.connect(overdue_count)
    }

    Component.onDestruction: {
        root.user_info_result.disconnect(detail_user)
        root.overdue_express_count_result.disconnect(overdue_count)
    }

    function button_inactive(text){
        if(text=="store"){
            courier_take_it_button.enabled = false 
            courier_take_it_button.show_text_color = txt_inactive
            courier_take_it_button.show_image = "img/courier88/btn_take_inactive.png"
        }
        else if(text=="take"){
            courier_take_package_button.enabled = false
            courier_take_package_button.show_text_color = txt_inactive
            courier_take_package_button.show_image = "img/courier88/btn_store_inactive.png"
        }
        else if(text=="overdue"){
            courier_take_overdue_button.enabled = false
            courier_take_overdue_button.show_text_color = txt_inactive
            courier_take_overdue_button.show_image = "img/courier88/btn_overdue_inactive.png"

        }
        else if(text=="opendoor"){
            access_door_button.enabled = false
            access_door_button.show_text_color = txt_inactive
            access_door_button.show_image = "img/courier88/btn_opendoor_inactive.png"
        }
        else if(text=="quitgui"){
            quit_gui_button.enabled = false
            quit_gui_button.show_text_color = txt_inactive
            quit_gui_button.show_image = "img/courier88/btn_quitgui_inactive.png"
        }
        else if(text=="reject"){
            courier_take_reject_button.enabled = false
            courier_take_reject_button.show_text_color = txt_inactive
            courier_take_reject_button.show_image = "img/courier88/btn_reject_inactive.png"
        }
    }

    // function store_inactive(){
    //     courier_take_it_button.enabled = false 
    //     courier_take_it_button.show_text_color = txt_inactive
    //     courier_take_it_button.show_image = "img/courier88/btn_take_inactive.png"
    // }

    // function take_inactive(){
    //     courier_take_package_button.enabled = false
    //     courier_take_package_button.show_text_color = txt_inactive
    //     courier_take_package_button.show_image = "img/courier88/btn_store_inactive.png"
    // }

    // function overdue_inactive(){
    //     courier_take_overdue_button.enabled = false
    //     courier_take_overdue_button.show_text_color = txt_inactive
    //     courier_take_overdue_button.show_image = "img/courier88/btn_overdue_inactive.png"
    // }

    // function quitgui_inactive(){
    //     quit_gui_button.enabled = false
    //     quit_gui_button.show_text_color = txt_inactive
    //     quit_gui_button.show_image = "img/courier88/btn_quitgui_inactive.png"
    // }

    // function opendoor_inactive(){
    //     access_door_button.enabled = false
    //     access_door_button.show_text_color = txt_inactive
    //     access_door_button.show_image = "img/courier88/btn_opendoor_inactive.png"
    // }

    function overdue_count(text){
        overdue_num.text = text
        // console.log("OVERDUE COUNT : " , text)
    }

    function detail_user(text){
        // console.log("DETAIL USER : ", text)
        var result = JSON.parse(text)
        courier_name = result.name
        courier_username = result.namelog
        courier_slice = result.namelog
        // console.log("USER Name TIPE : ", courier_name)
        courier_com = result.company_name
        courier_slice = courier_slice.slice(-3)
        // console.log("DETAIL USER : ", courier_com)
        if(courier_slice.indexOf("SPR") > -1){
            button_inactive("store")
            button_inactive("take")
            button_inactive("reject")
            button_inactive("quitgui")
        }else if(courier_slice.indexOf("ADM") > -1) {
            button_inactive("store")
            button_inactive("take")
            button_inactive("quitgui")
            // button_inactive("opendoor")
            button_inactive("reject")
            // console.log("USER ADMin TIPE : ", courier_slice)
        }else if(courier_slice.indexOf("OPS") > -1) {
            button_inactive("store")
            button_inactive("take")
            button_inactive("quitgui")
            button_inactive("opendoor")
            button_inactive("reject")
            // button_inactive("overdue")
            // console.log("USER OPeraSional TIPE : ", courier_slice)
        }else{
            button_inactive("store")
            button_inactive("take")
            button_inactive("quitgui")
            button_inactive("opendoor")
            button_inactive("reject")
            button_inactive("overdue")
            // console.log("USER OPeraSional TIPE : ", courier_slice)
        }
        // else if(result.company_name.indexOf("GRAB") > -1){
        //     courier_com = "GRAB Indonesia"
        // }else if(result.company_name.indexOf("Merchant") > -1){
        //     courier_take_reject_button.visible = false
        //     courier_take_it_button.visible = false
        //     courier_take_package_button.x = 328
        // }
        // slot_handler.start_video_capture("capture_" + result.namelog)
    }


    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Text {
        id: greeting_text
        x: 50
        y: 115
        width: 200
        height: 40
        font.family:"Microsoft YaHei"
        color:txt_color
        text: 'Hi, ' + courier_name + ' | ' + courier_com
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.PlainText
        font.pointSize:22
        font.capitalization: Font.Capitalize
    }


    FullWidthReminderText{
        y:210
        remind_color:txt_color
        remind_text:qsTr("Please select service options")
        remind_text_size:"35"
    }

    BackButtonAP{
        id:select_service_back_button
        x:20
        y:20
        // show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                dimm_act.visible = true
                notif_yes_no.visible = true
                // my_timer.stop()
                // my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
        }
    }

    CourierServiceUpButton{
        id:courier_take_package_button
        x:buttonXstore
        y:buttonY
        show_text_color: txt_button_color
        show_text:qsTr("Store")
        show_image:"img/courier88/btn_store.png"
        show_source:"img/button/1.png"
        // visible: false

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(courier_memory_view, {c_access:c_access, pref_login_user:pref_login_user})
            }
            onEntered:{
                courier_take_package_button.show_image = "img/courier88/btn_store_down.png"
                courier_take_package_button.show_text_color = "white"
            }
            onExited:{
                courier_take_package_button.show_image = "img/courier88/btn_store.png"
                courier_take_package_button.show_text_color = txt_button_color
                courier_take_package_button.show_source = "img/button/1.png"
            }
        }
    }

    CourierServiceUpButton{
        id:courier_take_overdue_button
        x:buttonXoverdue
        y:buttonY
        show_text_color: txt_button_color
        show_text:qsTr("Overdue")
        show_image:"img/courier88/btn_overdue.png"
        show_source:"img/button/1.png"
        // visible: (fromExpress=="Yes") ? false : true

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(background_overdue_time_view, {userAccess: courier_slice, courierUsername: courier_username, courierName: courier_name}) 
            }
            onEntered:{
                courier_take_overdue_button.show_image = "img/courier88/btn_overdue_down.png"
                courier_take_overdue_button.show_text_color = "white"
            }
            onExited:{
                courier_take_overdue_button.show_image = "img/courier88/btn_overdue.png"
                courier_take_overdue_button.show_text_color = txt_button_color
                courier_take_overdue_button.show_source = "img/button/1.png"
            }
            Text {
                id: overdue_num
                y: 10
                color: txt_button_color
                text: qsTr("0")
                anchors.right: parent.right
                anchors.rightMargin: 10
                styleColor: "#BF2E26"
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                font.bold: true
                font.pixelSize: 24
            }
        }
    }

    CourierServiceUpButton{
        id:courier_take_it_button
        x:buttonXtake
        y:buttonY
        show_text_color: txt_button_color
        show_text:qsTr("Take")
        show_image:"img/courier88/btn_take.png"
        show_source:"img/button/1.png"
        // visible: false

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(take_send_express_page)
            }
            onEntered:{
                courier_take_it_button.show_image = "img/courier88/btn_take_down.png"
                courier_take_it_button.show_text_color = "white"
            }
            onExited:{
                courier_take_it_button.show_image = "img/courier88/btn_take.png"
                courier_take_it_button.show_text_color = txt_button_color
                courier_take_it_button.show_source = "img/button/1.png"
            }
        }
    }

    CourierServiceUpButton{
        id:courier_take_reject_button
        x:buttonXreject
        y:buttonY
        show_text_color: txt_button_color
        show_text:qsTr("Reject")
        show_image:"img/courier88/btn_reject.png"
        show_source:"img/button/1.png"
        // visible: (fromExpress=="Yes" || c_access=="limited") ? false : true

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(take_reject_express)
            }
            onEntered:{
                courier_take_reject_button.show_image = "img/courier88/btn_reject_down.png"
                courier_take_reject_button.show_text_color = "white"
            }
            onExited:{
                courier_take_reject_button.show_image = "img/courier88/btn_reject.png"
                courier_take_reject_button.show_text_color = txt_button_color
                courier_take_reject_button.show_source = "img/button/1.png"
            }
        }
    }

    CourierServiceUpButton{
        id:access_door_button
        x:buttonXstore
        y:buttonY2
        show_text_color: txt_button_color
        show_text:qsTr("Akses Pintu")
        show_image:"img/courier88/btn_opendoor.png"
        show_source:"img/button/1.png"
        // visible: false

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(manager_cabinet_view, {userAccess: courier_slice, courierUsername: courier_username, courierName: courier_name})
            }
            onEntered:{
                access_door_button.show_image = "img/courier88/btn_opendoor_down.png"
                access_door_button.show_text_color = "white"
            }
            onExited:{
                access_door_button.show_image = "img/courier88/btn_opendoor.png"
                access_door_button.show_text_color = txt_button_color
                access_door_button.show_source = "img/button/1.png"
            }
        }
    }

    CourierServiceUpButton{
        id:quit_gui_button
        x:buttonXtake
        y:buttonY2
        show_text_color: txt_button_color
        show_text:qsTr("Keluar")
        show_image:"img/courier88/btn_quitgui.png"
        show_source:"img/button/1.png"
        // visible: false

        MouseArea {
            anchors.fill: parent
            onClicked: {
                slot_handler.start_explorer()
                Qt.quit()
            }
            onEntered:{
                quit_gui_button.show_image = "img/courier88/btn_quitgui_down.png"
                quit_gui_button.show_text_color = "white"
            }
            onExited:{
                quit_gui_button.show_image = "img/courier88/btn_quitgui.png"
                quit_gui_button.show_text_color = txt_button_color
                quit_gui_button.show_source = "img/button/1.png"
            }
        }
    }

    // CourierServiceDownButton{
    //     id:other_services_button
    //     x:517
    //     y:603
    //     show_text:qsTr("Other services")
    //     show_image:"img/courier10/otherservices1.png"
    //     show_source:"img/button/1.png"
    //     visible: (fromExpress=="Yes") ? false : true

    //     MouseArea {
    //         anchors.fill: parent
    //         onClicked: {
    //             if(press != "0"){
    //                 return
    //             }
    //             press = "1"
    //             my_stack_view.push(on_develop_view)
    //         }
    //         onEntered:{
    //             other_services_button.show_image = "img/bottondown/button_5.png"
    //             other_services_button.show_text_color = "white"
    //         }
    //         onExited:{
    //             other_services_button.show_image = "img/courier10/otherservices1.png"
    //             other_services_button.show_text_color = "#BF2E26"
    //             other_services_button.show_source = "img/button/1.png"
    //         }
    //     }
    // }

    // CourierServiceDownButton{
    //     id:history_button
    //     x:257
    //     y:603
    //     show_text:qsTr("History")
    //     show_image:"img/courier10/History1.png"
    //     show_source:"img/button/1.png"
    //     visible: (fromExpress=="Yes") ? false : true

    //     MouseArea {
    //         anchors.fill: parent
    //         onClicked: {
    //             if(press != "0"){
    //                 return
    //             }
    //             press = "1"
    //             my_stack_view.push(on_develop_view)
    //         }
    //         onEntered:{
    //             history_button.show_image = "img/bottondown/button_4.png"
    //             history_button.show_text_color = "white"
    //         }
    //         onExited:{
    //             history_button.show_image = "img/courier10/History1.png"
    //             history_button.show_text_color = "#BF2E26"
    //             history_button.show_source = "img/button/1.png"
    //         }
    //     }
    // }

    Item{
        id: how_to_use_info
        x: 105
        y:525
        width: 900
        height: 125
        anchors.horizontalCenter: parent.horizontalCenter
        visible: (fromExpress=="Yes") ? true : false
        property int item_font_size: 17
        property real opacity_rec: 0.4

        Rectangle{
            id: col_howto_1
            x:0
            color: "black"
            opacity: how_to_use_info.opacity_rec
            radius: 10
            height: parent.height
            width: (parent.width/2) - 5
        }
        Column{
            anchors.fill: col_howto_1
            spacing: 5
            Text{
                id: how_to_a1
                width: parent.width
                text: qsTr("TAKE :")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                color:"white"
                textFormat: Text.PlainText
                font.pointSize:22
            }
            Text{
                id: how_to_a2
                width: parent.width
                text: qsTr("Select this button to collect parcel/laundry from this locker.")
                wrapMode: Text.WordWrap
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                color:"white"
                textFormat: Text.PlainText
                font.pointSize:how_to_use_info.item_font_size
                font.italic: true
            }
        }

        Rectangle{
            id: col_howto_2
            color: "black"
            opacity: how_to_use_info.opacity_rec
            radius: 10
            x:(parent.width/2) + 10
            height: parent.height
            width: (parent.width/2) - 5
        }
        Column{
            anchors.fill: col_howto_2
            spacing: 5
            Text{
                id: how_to_b1
                width: parent.width
                text: qsTr("STORE :")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                color:"white"
                textFormat: Text.PlainText
                font.pointSize:22
            }
            Text{
                id: how_to_b2
                width: parent.width
                text: qsTr("Select this button to store parcels into this locker.")
                wrapMode: Text.WordWrap
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                color:"white"
                textFormat: Text.PlainText
                font.pointSize:how_to_use_info.item_font_size
                font.italic: true
            }
        }
    }

    LoadingView{
        id: loadingGif
    }

    HideWindow{
        id:secret_code_check
        visible: false

        Image {
            id: img_lock_secret
            x: 434
            y: 214
            width: 200
            height: 200
            source: "img/otherImages/lock_opened.png"
        }

        Text {
            x: 92
            y:386
            width: 848
            height: 200
            text: qsTr("Hi, secret code is enterred!\n Select Function Below?")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:40
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:secret_sure_button
            x:550
            y:576
            show_text:qsTr("Quit GUI")
            // show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    //my_stack_view.push(manager_service_view)
                    slot_handler.start_explorer()
                    Qt.quit()
                }
                onEntered:{
                    secret_sure_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    secret_sure_button.show_source = "img/button/7.png"
                }
            }
        }

        OverTimeButton{
            id:secret_admin_button
            x:200
            y:576
            show_text:qsTr("Access Doors")
            // show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(manager_cabinet_view) //,{press_time:1})
                }
                onEntered:{
                    secret_sure_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    secret_sure_button.show_source = "img/button/7.png"
                }
            }
        }

        BackButton{
            id:secret_back_button
            x:130
            y:230
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    // courier_login_username.bool = true
                    // courier_login_psw.bool = false
                    // courier_login_username.show_text=""
                    // courier_login_psw.show_text=""
                    // main_page.enabled = true
                    // courier_login_button.enabled = true
                    // secret_code_check.close()
                }
            }
        }

        Rectangle {
            id: camera_button
            x: 853
            y: 214
            width: 80
            height: 60
            color: "#00000000"
            anchors.topMargin: 0
            anchors.top: img_lock_secret.top
            Image {
                id: camera_image_button
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/item/camera.png"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(camera_capture_view)

                }
                onEntered:{
                    camera_image_button.source = "img/item/camera_red.png"
                }
                onExited:{
                    camera_image_button.source = "img/item/camera.png"
                }
            }
        }

        Rectangle {
            id: input_email
            x: 848
            y: 314
            width: 80
            height: 60
            color: "#00000000"
            anchors.topMargin: 70
            anchors.top: img_lock_secret.top
            Image {
                id: input_email_button
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/item/email.png"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(customer_input_email_view)

                }
                onEntered:{
                    input_email_button.source = "img/item/email-red.png"
                }
                onExited:{
                    input_email_button.source = "img/item/email.png"
                }
            }
        }

        Rectangle {
            id: test_settle_button
            x: 750
            y: 254
            width: 80
            height: 60
            color: "#00000000"
            anchors.topMargin: 0
            anchors.top: img_lock_secret.top
            Image {
                id: test_settle_button_image_button
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/courier08/08user.png"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log('test settle order')
                    slot_handler.start_push_data_settlement("TEST_SETTLEMENT")
                }
            }
        }

    }

    Rectangle{
        id:dimm_act
        visible:false
        enabled:true
        width: 1024
        height: 768
        color: "#AA4A4A4A"
    }

    Rectangle{
        id:notif_yes_no
        width: 650
        height: 400
        visible: false
        color: "white"
        radius: 22
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Text{
            id:text_yes_no
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: 0
            text: qsTr("Konfirmasi")
            anchors.top: parent.top
            anchors.topMargin: 50
            font.bold: true
            color:"#009BE1"
            font.family:"Microsoft YaHei"
            font.pixelSize:43
        }

        Text{
            id:isi_yes_no
            x: 93
            width: 850
            height: 50
            // text: (paymentCancel==true) ? qsTr("Transaksi kamu tidak dapat dibatalkan,\nkarena status pembayaran sudah sukses.") : qsTr("Apakah Anda yakin ingin membatalkan\ntransaksi?")
            text: qsTr("Apakah Anda yakin ingin keluar dari\nhalaman operator?")
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 200
            font.family:"Microsoft YaHei"
            color:"#4A4A4A"
            textFormat: Text.PlainText
            font.pixelSize: 26
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        NotifButtonAP{
            id: tidak_yes_no
            x: 50
            anchors.top: parent.top
            anchors.topMargin: 280
            buttonColor: 'BLUE'
            modeReverse: true
            buttonText: qsTr('TIDAK')

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = '0';
                    notif_yes_no.visible = false;
                    dimm_act.visible = false;
                }
            }
        }

        NotifButtonAP{
            id: oke_yes_no
            x: 350
            visible: true
            anchors.top: parent.top
            anchors.topMargin: 280
            buttonColor: 'BLUE'
            modeReverse: false
            buttonText: qsTr('YA')

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }
}
