import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    width: 1024
    height: 768
    property int timer_value: 60
    property var locker_group: ["The Suite Metro Bandung", "Apt. 19 Avenue"]
    property bool isHomtel: false

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.start_get_locker_name()
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.start_get_locker_name_result.connect(define_homtel)
    }

    Component.onDestruction: {
        root.start_get_locker_name_result.disconnect(define_homtel)
    }

    function define_homtel(text){
        if(text == ""){
            return
        }
        if(locker_group.indexOf(text) > -1){
            isHomtel = true
        }else{
            isHomtel = false
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    FullWidthReminderText{
        id:please_select_merchant_name1
        x: 0
        y:119
        remind_text:qsTr("Available merchant :")
        remind_text_size:"40"
    }

    FullWidthReminderText{
        id:please_select_merchant_name2
        x: 0
        y:712
        remind_text:qsTr("*Click merchants logo for returning parcel")
        remind_text_size:"20"
    }

    RejectSelectMerchantNameButton{
        id:zalora_button
        x:636
        y:364
        scale: 0.8
        show_image:"img/returnstep/zalora-white.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                slot_handler.start_customer_reject_select_merchant("ZALORA")
                my_stack_view.push(reject_input_memory_page)
            }
            onEntered:{
                zalora_button.show_image = "img/returnstep/zalora-red.png"
            }
            onExited:{
                zalora_button.show_image = "img/returnstep/zalora-white.png"
            }
        }
    }

   RejectSelectMerchantNameButton{
        id:lazada_button
        x:636
        y:171
        scale: 0.8
        show_image:"img/returnstep/lazada-white.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                slot_handler.start_customer_reject_select_merchant("LAZADA")
                my_stack_view.push(reject_input_memory_page)
            }
            onEntered:{
                lazada_button.show_image = "img/returnstep/lazada-red.png"
            }
            onExited:{
                lazada_button.show_image = "img/returnstep/lazada-white.png"
            }
        }
    }

    RejectSelectMerchantNameButton{
        id:mm_button
        x:140
        y:364
        scale: 0.8
        show_image:"img/returnstep/MM-white.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                 my_stack_view.push(mm_return_info)
            }
            onEntered:{
                mm_button.show_image = "img/returnstep/MM-red.png"
            }
            onExited:{
                mm_button.show_image = "img/returnstep/MM-white.png"
            }
        }
    }

    RejectSelectMerchantNameButton{
        id:oriflame_button
        x:390
        y:364
        scale: 0.8
        show_image:"img/returnstep/oriflame-white.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                slot_handler.start_customer_reject_select_merchant("ORIFLAME")
                my_stack_view.push(reject_input_memory_page)
            }
            onEntered:{
                oriflame_button.show_image = "img/returnstep/oriflame-red.png"
            }
            onExited:{
                oriflame_button.show_image = "img/returnstep/oriflame-white.png"
            }
        }
    }

    RejectSelectMerchantNameButton{
        id:javamifi_button
        x:390
        y:171
        scale: 0.8
        show_image:"img/returnstep/javamifi-white.png"
        visible: !isHomtel

        MouseArea {
            anchors.fill: parent
            onClicked: {
                slot_handler.start_customer_reject_select_merchant("JAVAMIFI")
                my_stack_view.push(reject_input_memory_page)
            }
            onEntered:{
                javamifi_button.show_image = "img/returnstep/javamifi-red.png"
            }
            onExited:{
                javamifi_button.show_image = "img/returnstep/javamifi-white.png"
            }
        }
    }


    RejectSelectMerchantNameButton{
        id:blibli_button
        x:140
        y:171
        scale: 0.8
        show_image:"img/returnstep/blibli-white.jpg"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                slot_handler.start_customer_reject_select_merchant("BLIBLI")
                my_stack_view.push(reject_input_memory_page, {show_text:"RMA"})
            }
            onEntered:{
                blibli_button.show_image = "img/returnstep/blibli-red.jpg"
            }
            onExited:{
                blibli_button.show_image = "img/returnstep/blibli-white.jpg"
            }
        }
    }

    RejectSelectMerchantNameButton{
        id:homtel_button
        x:390
        y:171
        scale: 0.8
        show_image:"img/returnstep/homtel-white.png"
        visible: isHomtel

        MouseArea {
            anchors.fill: parent
            onClicked: {
                slot_handler.start_customer_reject_select_merchant("HOMTEL")
                my_stack_view.push(reject_input_memory_page)
            }
            onEntered:{
                homtel_button.show_image = "img/returnstep/homtel-red.png"
            }
            onExited:{
                homtel_button.show_image = "img/returnstep/homtel-white.png"
            }
        }
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                back_button.show_source = "img/bottondown/error_down.png"
            }
            onExited:{
                back_button.show_source = "img/05/button.png"
            }
        }
    }

    Text {
        id: step_how
        x: 50
        y: 576
        color: "#ffffff"
        text: qsTr("Steps to return parcel :")
        font.pixelSize: 30
        textFormat: Text.PlainText
        font.family:"Microsoft YaHei"
        verticalAlignment: Text.AlignTop
        horizontalAlignment: Text.AlignHCenter

        Text {
            id: num_how1
            x: 0
            y: 35
            color: "#ffffff"
            text: qsTr("1.")
            font.bold: false
            textFormat: Text.PlainText
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 28
            visible: step_how.visible

            Text {
                id: text_how1
                x: 32
                y: 0
                width: 748
                height: 34
                color: "#ffffff"
                text: qsTr("Fill out Merchant Return Form.")
                verticalAlignment: Text.AlignVCenter
                font.italic: true
                font.family:"Microsoft YaHei"
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
                font.pixelSize: 22
                visible: step_how.visible

            }
        }

        Text {
            id: num_how2
            x: 0
            y: 60
            color: "#ffffff"
            text: qsTr("2.")
            font.bold: false
            textFormat: Text.PlainText
            verticalAlignment: Text.AlignTop
            font.pixelSize: 28
            font.family:"Microsoft YaHei"
            horizontalAlignment: Text.AlignHCenter
            visible: step_how.visible


            Text {
                id: text_how2
                x: 32
                y: 0
                width: 752
                height: 34
                color: "#ffffff"
                text: qsTr("Scan or Enter the Package ID.")
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 22
                font.family:"Microsoft YaHei"
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
                font.italic: true
                visible: step_how.visible

            }
        }

        Text {
            id: num_how3
            x: 0
            y: 90
            color: "#ffffff"
            text: qsTr("3.")
            font.bold: false
            textFormat: Text.PlainText
            verticalAlignment: Text.AlignTop
            font.pixelSize: 28
            font.family:"Microsoft YaHei"
            horizontalAlignment: Text.AlignHCenter
            visible: step_how.visible


            Text {
                id: text_how3
                x: 32
                y: 0
                width: 752
                height: 34
                color: "#ffffff"
                text: qsTr("Select size and drop parcel to locker.")
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 22
                font.family:"Microsoft YaHei"
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
                font.italic: true
                visible: step_how.visible

            }
        }
    }
}
