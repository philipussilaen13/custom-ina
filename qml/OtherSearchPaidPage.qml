import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: other_search_paid_page

    property int timer_value: 30
    property var show_time:""
    property var show_time_for_text:""
    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            slot_handler.start_get_popbox_express_info()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc .counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    DoorButton{
        id:back_button
        y:604
        x:370
        show_text:qsTr("OK")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                back_button.show_image = "img/door/2.png"
            }
            onExited:{
                back_button.show_image = "img/door/1.png"
            }
        }
    }

    Column {
        x: 0
        y: 280
        spacing: 15

        FullWidthReminderText{
            id:text
            remind_text:qsTr("This order number is ")
            remind_text_size:"35"
        }

        FullWidthReminderText{
            remind_text:show_time_for_text
            remind_text_size:"35"
        }

        FullWidthReminderText {
            id: text1
            remind_text_size: "35"
            remind_text: qsTr("This  is ")
        }

        FullWidthReminderText{
            remind_text:qsTr(".....")
            remind_text_size:"35"
        }
    }

    Component.onCompleted: {
        root.start_get_popbox_express_info_result.connect(handle_text)
    }

    Component.onDestruction: {
        root.start_get_popbox_express_info_result.disconnect(handle_text)
    }

    function handle_text(result){
        if(result == ""){
            return
        }
        var obj = JSON.parse(result)
        var str_time = obj.order_date.replace(/-/g,"/")
        var show_time = new Date(str_time)
        var time_Y = add_zero(show_time.getFullYear());
        var mouth = (show_time.getMonth()+1 < 10 ? +(show_time.getMonth()+1) : show_time.getMonth()+1)
        switch (mouth){
        case 1: var time_M = "January"
            break;
        case 2: var time_M = "February"
            break;
        case 3: var time_M = "March"
            break;
        case 4: var time_M = "April"
            break;
        case 5: var time_M = "May"
            break;
        case 6: var time_M = "June"
            break;
        case 7: var time_M = "July"
            break;
        case 8: var time_M = "August"
            break ;
        case 9: var time_M = "September"
            break;
        case 10: var time_M = "October"
            break;
        case 11: var time_M = "November"
            break;
        case 12: var time_M = "December"
            break;
        }
        var time_D = add_zero(show_time.getDate());
        var time_h = add_zero(show_time.getHours());
        var time_m = add_zero(show_time.getMinutes());
        var time_s = add_zero(show_time.getSeconds());
        show_time_for_text = time_D+" "+time_M+" "+time_Y+" "+time_h+":"+time_m+":"+time_s
    }

    function add_zero(temp){
        if(temp<10) return "0"+temp;
        else return temp;
    }

    function change_mouth(mouth){
        switch (mouth){
        case "1": print("January")
            break;
        case "2": print("February")
            break;
        case "3": print("March")
            break;
        case "4": print("April")
            break;
        case "5": print("May")
            break;
        case "6": print("June")
            break;
        case "7": print("July")
            break;
        case "8": print("August")
            break ;
        case "9": print("September")
            break;
        case "10": print("October")
            break;
        case "11": print("November")
            break;
        case "12": print("December")
            break;
        }
    }
}
