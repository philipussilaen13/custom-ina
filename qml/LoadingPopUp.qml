import QtQuick 2.4

Rectangle{
    id: main_rectangle
    color: "#4A4A4A4A"
    property alias popupText: notif_text.text
    width: 1024
    height: 768
    visible: false

    Rectangle{
        id:sub_rec_dimm
        width: 650
        height: 500
        color: "white"
        radius: 22
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Text{
            id: notif_text
            x: 86
            y: 179
            width: 450
            height: 100
            font.family: 'Microfot YaHei'
            font.pixelSize: 30
            color: '#009BE1'
            text: qsTr("Memproses...")
            anchors.verticalCenterOffset: 120
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
        }

        AnimatedImage{
            id: notif_img
            width: 396
            height: 218
            anchors.verticalCenterOffset: 0
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            source: 'img/apservice/spin_pop.gif'
        }
    }


    function open(){
        main_rectangle.visible = true;
    }

    function close(){
        main_rectangle.visible = false;
    }

}





