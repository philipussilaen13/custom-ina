import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:88
    height:88
    color:"#ffc125"
    radius: 22
    property var slot_text:""
    property var show_image:""

    Image{
        id:function_button_Image
        width:88
        height:88
        source:""
    }
    Text{
        text:slot_text
        color:"#ffffff"
        font.family:"Microsoft YaHei"
        font.pixelSize:20
        anchors.centerIn: parent;
        font.bold: true

    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if(slot_text != "delete"){
                full_keyboard.function_button_clicked(slot_text)
            }
            else
                full_keyboard.letter_button_clicked("")
        }
        onEntered:{
            //function_button_Image.source = "img/button/Functiondown.png" -> Disable Background Change
        }
        onExited:{
            //function_button_Image.source = "img/button/KeyButton_2.png" -> Disable Background Change
        }
    }

}
