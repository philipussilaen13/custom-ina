import QtQuick 2.4
import QtQuick.Controls 1.2
import QtWebKit 3.0

Background{
    id: base_bground
    width: 1024
    height: 768

    property var first_load: "Yes"
    property var faqUrl: "http://localhost/faq/"
    property var groUrl: "http://localhost/grocery/"
    property var mapUrl: "https://www.popbox.asia/locker/maps/id/jakarta%20barat"
    property var locker_name: "---"
    property var gui_version: "---"
    property var cs_line: "CS Line: 021-29022537/38"
    property int loading_time: 5
    property int tvc_timeout: 60
    property bool vss_mode: true // To control 15 inch VSS Scrensaver mode
    property var reference: root.reference
    property bool isEmergency: root.isEmergency


    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            //console.log(Qt.formatDateTime(new Date(), "yyyy-MM-dd hh:mm"))
            slot_handler.start_get_gui_version()
            slot_handler.start_get_locker_name()
            slot_handler.start_get_free_mouth_mun()
            slot_handler.start_idle_mode()
            select_first_button.show_source = "img/button/9.png"
            select_second_button.show_source = "img/button/9.png"
            timer_clock.restart()
            if(vss_mode==true){
                tvc_loading.counter = tvc_timeout
                show_tvc_loading.start()
                loading_window.visible = true
                show_timer_loading.start()
                main_page.enabled = false
                if(first_load=="No" && reference == "people_count"){
                    loading_time = 3
                 }
            }
        }
        if(Stack.status==Stack.Deactivating){
            show_tvc_loading.stop()
            timer_clock.stop()
        }
    }

    Component.onCompleted: {
        root.start_get_gui_version_result.connect(get_gui_version)
        root.start_get_locker_name_result.connect(show_locker_name)
        root.free_mouth_result.connect(show_free_mouth_num)
//        root.start_get_detection_result.connect(get_detection)
    }

    Component.onDestruction: {
        root.start_get_gui_version_result.disconnect(get_gui_version)
        root.start_get_locker_name_result.disconnect(show_locker_name)
        root.free_mouth_result.disconnect(show_free_mouth_num)
//        root.start_get_detection_result.disconnect(get_detection)
    }

    function get_gui_version(result){
        console.log("version : ", result)
        gui_version = result;
    }

    MouseArea{
        id: free_touch
        anchors.fill: parent
        onClicked: slot_handler.start_post_activity("FreeTouch")
    }

    Rectangle{
        id: rec_subs_button
        x: 928
        y: 161
        width: 180
        height: 131
        color: "white"
        radius: 65
        anchors.right: parent.right
        anchors.rightMargin: -84
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 476
        border.width: 0
        AnimatedImage{
            x: 4
            y: 3
            source: "img/otherImages/register-here-bak.gif"
            fillMode: Image.PreserveAspectFit
        }
        MouseArea{
            anchors.fill: rec_subs_button
            onClicked: {
                my_stack_view.push(subscribe_input_detail_view, {usageOf: "subscribe_data"})
                slot_handler.start_post_activity("SubscribeTouch")
            }
        }
    }

    Rectangle{
        id: main_page
        width: base_bground.width
        height: base_bground.height
        color: "transparent"

        FullWidthReminderText{
            y:310
            remind_text:qsTr("Please select main language")
            remind_text_size:"45"
        }

        FullWidthReminderText{
            y:350
            remind_text:qsTr("Please select second language")
            remind_text_size:"25"
        }

        SelectLanguageButton{
            id:select_first_button
            x:234
            y:455
            show_text:qsTr("main language")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    slot_handler.select_language("first.qm")
                    my_stack_view.push(select_service_b_view, {faqUrl:faqUrl,groUrl:groUrl})
                    slot_handler.clean_user_token()
                    first_load = "No"
                    slot_handler.start_post_activity("SelectLanguage1")
                    slot_handler.stop_idle_mode()
                }
                onEntered:{
                    select_first_button.show_source = "img/bottondown/buttondown_1.png"
                }
                onExited:{
                    select_first_button.show_source = "img/button/9.png"
                }
            }

            Image {
                id: flag1
                x: 0
                y: 0
                width: 64
                height: 64
                fillMode: Image.PreserveAspectFit
                source: "img/indonesia_flag_100.png"
            }
        }

        SelectLanguageButton{
            id:select_second_button
            x:515
            y:455
            show_text:qsTr("second language")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    slot_handler.select_language("second.qm")
                    my_stack_view.push(select_service_e_view, {faqUrl:faqUrl,groUrl:groUrl})
                    slot_handler.clean_user_token()
                    first_load = "No"
                    slot_handler.start_post_activity("SelectLanguage2")
                    slot_handler.stop_idle_mode()
                }
                onEntered:{
                    select_second_button.show_source = "img/bottondown/buttondown_1.png"
                }
                onExited:{
                    select_second_button.show_source = "img/button/9.png"
                }
            }

            Image {
                id: flag2
                x: 0
                y: 0
                width: 64
                height: 64
                fillMode: Image.PreserveAspectFit
                source: "img/uk_flag_100.png"
            }
        }

        Row {
            id: avail_doors_view
            x: 240
            y: 550
            anchors.horizontalCenter: main_page.horizontalCenter
            spacing: 20

            Text {
                id: mouth_title
                text: qsTr("Available Lockers: ")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Row {
                spacing: 5
                Text {
                    id: mini_type
                    text: qsTr("XS :")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

                Text {
                    id: mini_num
                    text: qsTr("0")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }
            }

            Row {
                spacing: 5
                Text {
                    id: small_type
                    text: qsTr("S :")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

                Text {
                    id: small_num
                    text: qsTr("0")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }
            }

            Row {
                spacing: 5
                Text {
                    id: mid_type
                    text: qsTr("M :")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

                Text {
                    id: mid_num
                    text: qsTr("0")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }
            }

            Row {
                spacing: 5
                Text {
                    id: big_type
                    text: qsTr("L :")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

                Text {
                    id: big_num
                    text: qsTr("0")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }
            }

            Row {
                spacing: 5
                Text {
                    id: extra_big_type
                    text: qsTr("XL:")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

                Text {
                    id: extra_big_num
                    text: qsTr("0")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

            }
        }
    }

    function show_free_mouth_num(text){
        var obj = JSON.parse(text)
        extra_big_num.text = 0
        big_num.text = 0
        mid_num.text = 0
        small_num.text = 0
        mini_num.text = 0
        for(var i in obj){
            if(i == "XL"){
                extra_big_num.text = obj[i]
            }
            if(i == "L"){
                big_num.text = obj[i]
            }
            if(i == "M"){
                mid_num.text = obj[i]
            }
            if(i == "S"){
                small_num.text = obj[i]
            }
            if(i == "MINI"){
                mini_num.text = obj[i]
            }
        }
    }

    function show_locker_name(text){
        if(text == ""){
            return
        }
        locker_name = text
    }

//    function get_detection(result){
//        console.log("get_detection : ", JSON.stringify(result))
//        var detection_result = JSON.parse(result)
//    }


    WebView{
        id: preload_webview_maps
        visible: false
        enabled: false
        url: mapUrl
    }
  /*WebView{
        id: webviewGRO
        visible: false
        enabled: false
        url: groUrl
    }*/

    Rectangle{
        id: loading_window
        x: 66
        y: 129
        color: "white"
        opacity: 0.9
        width: 700
        height: 400
        border.width: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Text {
            id: please_wait_text
            x: 137
            y: 30
            width: 400
            height: 35
            text: " Loading Modules..."
            wrapMode: Text.WordWrap
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:35
            color:"#c50808"
            visible: (reference=="none") ? true : false
        }

        Text {
            id: please_welcome
            x: 137
            y: 30
            width: 400
            height: 350
            text: "Hi There, Welcome to PopBox Locker @ " + locker_name
            wrapMode: Text.WordWrap
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:35
            color:"#c50808"
            visible: (reference=="none") ? false : true
        }

        Rectangle{
            id: timer_rec
            width: 10
            height: 10
            x:0
            y:0
            visible: false
            QtObject{
                id:time_loading
                property int counter
                Component.onCompleted:{
                    time_loading.counter = loading_time
                }
            }
            Timer{
                id:show_timer_loading
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    time_loading_text.text = time_loading.counter
                    time_loading.counter -= 1
                    if(time_loading.counter==(loading_time-1)){
                        slot_handler.start_get_gui_version()
                    }
                    if(time_loading.counter < 0){
                        show_timer_loading.stop()
                        loading_window.visible = false
                        main_page.enabled = true
                        first_load = "No"
                        slot_handler.start_get_locker_name()
                        slot_handler.start_get_free_mouth_mun()
                    }
                }
            }
        }

        AnimatedImage{
            id: loading_image
            width: 200
            height: 200
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            fillMode: Image.PreserveAspectFit
            source: "img/item/progress-circle.gif"
            visible: (reference=="none") ? true : false

            Text {
                id:time_loading_text
                anchors.centerIn: parent
                width: 50
                height: 50
                font.family:"Microsoft YaHei"
                color:"#c50808"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                textFormat: Text.PlainText
                font.pointSize:35
                wrapMode: Text.WordWrap
                visible: (reference=="none") ? true : false
            }
        }

        Text {
            id: show_gui_version_text
            x: 137
            y: 345
            width: 400
            height: 35
            text: 'GUI : ' + gui_version
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:25
            color:"#c50808"
            visible: (reference=="none" && gui_version!="---") ? true : false

        }
    }

        Rectangle{
            id: timer_tvc
            width: 10
            height: 10
            x:0
            y:0
            visible: false
            QtObject{
                id:tvc_loading
                property int counter
                Component.onCompleted:{
                    tvc_loading.counter = tvc_timeout
                }
            }
            Timer{
                id:show_tvc_loading
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    tvc_loading.counter -= 1
                    if(tvc_loading.counter == 0){
                        show_tvc_loading.stop();
                        first_load = "No";
                        //slot_handler.set_tvc_player("START")
                        console.log("starting tvc player...")
                        my_stack_view.push(ad_page, {mode:"mediaPlayer"})
                        if (isEmergency) my_stack_view.push(emergency_view, {screenSize:'15'});
                        //tvc_loading.counter = tvc_timeout
                        //show_tvc_loading.restart()
                    }
                }
            }
        }

    Text {
        id: locker_name_text
        x: 262
        y: 690
        width: 500
        height: 40
        text: locker_name
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        font.pixelSize:25
        color:"#ffffff"
    }

    Text {
        id: cs_line_text
        x: 753
        y: 720
        width: 250
        height: 20
        text: cs_line
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        font.pixelSize:18
        color:"#ffffff"
        visible: false
    }

    Rectangle{
        id : bground_time
        x: -16
        y: 29
        color: "white"
        radius: 20
        opacity: 0.85
        width: 300
        height: 80
    }

    Text {
        id: timeText
        x: 15
        y: 39
        width: 150
        height: 35
        text: new Date().toLocaleTimeString(Qt.locale("id_ID"), "hh:mm:ss")
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        font.pixelSize:30
        color:"darkred"
    }

    Text {
        id: dateText
        x: 15
        y: 75
        height: 25
        text: new Date().toLocaleDateString(Qt.locale("id_ID"), Locale.LongFormat)
        font.italic: false
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        font.pixelSize:20
        color:"darkred"
    }

    Timer {
        id: timer_clock
        interval: 1000
        repeat: true
        running: true
        onTriggered:
        {
            timeText.text = new Date().toLocaleTimeString(Qt.locale("id_ID"), "hh:mm:ss")
        }
    }

}
