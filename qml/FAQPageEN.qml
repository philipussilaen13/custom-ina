import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:faq_en
    property int timer_value: 60
    property var initurl: ""

    Stack.onStatusChanged:{
       if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }


    BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
                onEntered:{
                    select_service_back_button.show_source = "img/bottondown/down.2.png"
                }
                onExited:{
                    select_service_back_button.show_source = "img/button/7.png"
                }
            }
    }

    Rectangle{
        id: timer_base
        width:39
        height:11
        y:10
        color:"transparent"
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                countShow.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
        Text{
            id: countShow
            x:15
            y:15
            visible: false
            color:"#FFFF00"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:16
        }
    }

    Item{
        id: container
        x: 0
        y: 160
        width: 980
        height: 600
        focus: true

        ScrollBarPop{
            id: vertical_sbar
            y: 0
            x: container.width + 10
            flickable: contents
            height: container.height
            color: "white"
        }

        Flickable{
            id: contents
            anchors.fill: container
            interactive: true
            flickDeceleration: 750
            maximumFlickVelocity: 1500
            boundsBehavior: Flickable.StopAtBounds
            contentHeight: rows_group.height
            contentWidth: rows_group.width
            clip: true
            focus: true

            Column{
                id: rows_group
                spacing: 5

                //==========================
                TextTemplate{
                    content: "1. What is PopBox Locker ?";
                    style: "Header";
                }
                TextTemplate{
                    content: "PopBox Locker (Electronic Locker) is a safe and convenient way to pick up deliveries and returning parcels.";
                }
                //==========================
                TextTemplate{
                    content: "2. Why use PopBox Locker ?";
                    style: "Header";
                }
                TextTemplate{
                    content: "Easy to use, cheaper delivery rate, faster delivery time, safe and convenient.";
                }
                //==========================
                TextTemplate{
                    content: "3. How to  use PopBox Locker ?";
                    style: "Header";
                }
                TextTemplate{
                    content: "- Buy from our merchants.";
                    leftPadding: 75;
                }
                TextTemplate{
                    content: "- Select PopBox locker as delivery location & pick your nearest locker.";
                    leftPadding: 75;
                }
                TextTemplate{
                    content: "- When item has arrived, you will receive SMS with 6 digit PIN code to retrieve the item.";
                    leftPadding: 75;
                }
                TextTemplate{
                    content: "- Go to the locker, input the pin and enjoy your parcel.";
                    leftPadding: 75;
                }
                //==========================
                TextTemplate{
                    content: "4. Where are PopBox Locker Locations ?";
                    style: "Header";
                }
                TextTemplate{
                    content: "- Shopping Malls.";
                    leftPadding: 75;
                }
                TextTemplate{
                    content: "- Train Stations.";
                    leftPadding: 75;
                }
                TextTemplate{
                    content: "- Office Buildings.";
                    leftPadding: 75;
                }
                TextTemplate{
                    content: "- Apartments / Residential Suites.";
                    leftPadding: 75;
                }
                TextTemplate{
                    content: "- Convenient Stores.";
                    leftPadding: 75;
                }
                TextTemplate{
                    content: "- Universities.";
                    leftPadding: 75;
                }
                //==========================
                TextTemplate{
                    content: "5. Who are Our Partners ?";
                    style: "Header";
                }
                TextTemplate{
                    content: "";
                    style: "container";
                    globalHeight: 400;
                    Column{
                        Row{
                            Image{
                                id: img1
                                scale: 0.9
                                source: "img/faq/asmaraku.png"
                                fillMode: Image.Stretch
                            }
                            Image{
                                id: img2
                                scale: 0.9
                                source: "img/faq/lazada.png"
                                fillMode: Image.Stretch
                            }
                            Image{
                                id: img3
                                scale: 0.9
                                source: "img/faq/zalora.png"
                                fillMode: Image.Stretch
                            }
                        }
                        Row{
                            Image{
                                id: img4
                                scale: 0.9
                                source: "img/faq/sukaoutdoor.png"
                                fillMode: Image.Stretch
                            }
                            Image{
                                id: img5
                                scale: 0.9
                                source: "img/faq/mm.png"
                                fillMode: Image.Stretch
                            }
                            Image{
                                id: img6
                                scale: 0.9
                                source: "img/faq/seroyamart.png"
                                fillMode: Image.Stretch
                            }
                        }
                        Row{
                            Image{
                                id: img7
                                scale: 0.9
                                source: "img/faq/frozen.png"
                                fillMode: Image.Stretch
                            }
                            Image{
                                id: img8
                                scale: 0.9
                                source: "img/faq/jne.png"
                                fillMode: Image.Stretch
                            }
                            Image{
                                id: img9
                                scale: 0.9
                                source: "img/faq/kudo.png"
                                fillMode: Image.Stretch
                            }
                        }
                    }

                }
                //==========================
                TextTemplate{
                    content: "6. How can I get further information ?";
                    style: "Header";
                }
                TextTemplate{
                    content: "Customer Service at +62 21 22538719 or email to info@popbox.asia";
                }
                //==========================
                TextTemplate{
                    content: " ";
                }
                //==========================

                }
            }
    }
}


